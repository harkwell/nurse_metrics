Khallware NurseMetrics
=================
Overview
---------------
Record and analyze your baby's everyday life events. This application
empowers you to collect information that most doctors request at well
visits.  Set alarms and be alerted to recurring events. Graph such trending
information as: diaper use, nap length and consumption.

First-time Only
---------------

```shell
chromium-browser https://developer.android.com/studio/index.html
# download android studio ide
sudo unzip -d /usr/local android-studio-ide-*-linux.zip
rm android-studio-ide-*-linux.zip
export PATH=$PATH:/usr/local/android-studio/bin/
studio.sh
export PATH=$PATH:$HOME/Android/Sdk/tools/
android
# Android SDK build-tools v22.0.1
# Android 5.1.1 - SDK Platform v2
# Android 5.1.1 - Google APIs v1
# Android 5.1.1 - Intel x86 Atom_64 System Image
# accept license and install
android update sdk --no-ui --obsolete --force
```

If you are running under centos7 minimal and have the required dependencies
already, ignore the following:

```shell
docker run -it -h build-nursemetrics --name build-nursemetrics -v $HOME/Android:/usr/local/Android centos
yum install -y git maven-3.0.5 java-1.8.0-openjdk-devel
#yum install -y epel-release
#yum install -y git zlib.i686 ncurses-libs.i686 bzip2-libs.i686
mkdir -p ~/.m2/
cat <<'EOF' >~/.m2/settings.xml
<settings xmlns="http://maven.apache.org/POM/4.0.0"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
      http://maven.apache.org/xsd/settings-1.0.0.xsd">
   <profiles>
      <profile>
         <id>bnursemetrics-properties</id>
         <properties>
            <android.sdk.path>/usr/local/Android/Sdk</android.sdk.path>
         </properties>
      </profile>
   </profiles>
   <activeProfiles>
      <activeProfile>bnursemetrics-properties</activeProfile>
   </activeProfiles>
</settings>
EOF
```


Build
---------------

```shell
git clone https://gitlab.com/harkwell/nurse_metrics.git && cd nurse_metrics
export ANDROID_HOME=/home/khall/Android/Sdk/
mvn package
ls -ld target/NurseMetrics.apk
```

Deploy
---------------

```shell
# first time
android create avd -n fovea -t 26
android list targets
mksdcard 256M ~/tmp/sdcard1.iso
emulator -sdcard ~/tmp/sdcard1.iso -avd fovea
# on phones, don't forget to set "settings" -> "security" -> "unknown sources"

# re-install
adb uninstall com.khallware.nurseMetrics
adb install target/NurseMetrics.apk
```

Releases
---------------

```shell
# tag release (eg "nursemetrics_1_1")
# checkout source from tag

git checkout nursemetrics_1_1
SDKVER=$(android list targets |grep ^id: |tail -1 |awk '{print $2}')
mvn package -Dbuild.apkver=$(date +%Y%m%d) -Dbuild.sdkver=$SDKVER -Dbuild.incr=$(date +%Y%m%d)-$((count++))

# release apk
PASS=MYPASSWORD
cp keystore.jks /tmp/
mvn package -Dbuild.apkver=$(date +%Y%m%d) -Dbuild.sdkver=$SDKVER -Dbuild.incr=$(date +%Y%m%d)-$((count++)) -Dandroid.release=true -Dkeystore.password=$PASS -Dsigning-keystore-alias=khallware.com -Psign
```
