CHK root view contains buttons for: add/select child, diapers, naps, schedule, bottle, feed times, and analysis
CHK root view has menu option for: about
CHK select child view contains: new child entry form, children list, add child context menu option
CHK diaper view contains: diaper change event history, new diaper change data entry
CHK nap view contains: sleep event history, and new sleep event data entry
CHK schedule view contains: recurring events alarm definition
CHK bottle view contains: bottle event history, new bottle event data entry
CHK feed time view contains: feeding time history list, new feed time data entry with now button and stop watch
CHK analysis view will use achartengine 0.4.0 http://www.achartengine.org/
CHK analysis view contains: separate graphs for various metrics (diapers, naps, bottle over time)
CHK app should support multiple children
CHK when a male child is chosen, button backgrounds are blue, pink otherwise
CHK very nice graphics
