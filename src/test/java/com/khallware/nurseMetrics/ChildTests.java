package com.khallware.nurseMetrics;

import android.database.Cursor;
import junit.framework.Assert;
import org.junit.Test;

public class ChildTests
{
	@Test
	public void testNewChildren()
	{
		boolean retval = true;
		String[] boys = new String[] {
			"John", "Slim", "Joe", "Slick", "Bob", "Kyle", "Jack",
			"Tom", "Doug", "Felix", "Henry", "Lary", "Moe", "Ned"
		};
		String[] girls = new String[] {
			"Tina", "Beth", "Ashley", "Carrie", "Dawn", "Fae",
			"Lucy", "Trish", "Becky", "Donna", "Gretchen", "Shela"
		};
		for (String kid : boys) {
			Child c = new Child();
			c.name = kid;
			c.setBoy();
			retval &= c.readyToAdd();
		}
		for (String kid : girls) {
			Child c = new Child();
			c.name = kid;
			c.setGirl();
			retval &= c.readyToAdd();
		}
		Assert.assertTrue(retval);
	}
}
