package com.khallware.nurseMetrics;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.Calendar;
import junit.framework.Assert;

public class Schedule extends AbstractEvent
{
	private static final String TAG = Constants.TAG_SCHEDULE;
	public static enum alarmType
	{
		GENERIC, WAKE, SLEEP, FEED, MEDICAL, BATH
	}

	private long alarmTime = System.currentTimeMillis()/1000;
	private boolean repeats = false;
	private alarmType myType = alarmType.GENERIC;
	private boolean active = false;

	public Schedule()
	{
		Log.d(TAG, "constructing schedule object");
	}

	public Schedule(Parcel p1_input)
	{
		super(p1_input);
		boolean tmp[] = { false, false };
		Log.d(TAG, "constructing schedule object from Parcel");
		alarmTime = p1_input.readLong();
		p1_input.readBooleanArray(tmp);
		repeats = tmp[0];
		myType = alarmType.values()[p1_input.readInt()];
		active = tmp[1];
	}

	@Override
	public void writeToParcel(Parcel p1_out, int p2_flags)
	{
		super.writeToParcel(p1_out, p2_flags);
		boolean tmp[] = { repeats, active };
		p1_out.writeLong(alarmTime);
		p1_out.writeBooleanArray(tmp);
		p1_out.writeInt(myType.ordinal());
	}

	public static final Parcelable.Creator<Schedule> CREATOR = 
		new Parcelable.Creator<Schedule>() {
			public Schedule createFromParcel(Parcel p1_input) {
				return(new Schedule(p1_input));
			}
			public Schedule[] newArray(int p1_size) {
				return(new Schedule[p1_size]);
			}
		};

	public long getAlarmTime()
	{
		Assert.assertTrue(alarmTime >= 0);
		return(alarmTime);
	}

	public void setAlarmTime(long p1_time)
	{
		Assert.assertTrue(p1_time > 0);
		alarmTime = p1_time;
		setEpochTime(alarmTime);
	}

	public alarmType getAlarmType()
	{
		return(myType);
	}

	public void setAlarmType(alarmType p1_type)
	{
		myType = p1_type;
	}

	public void setActive(boolean p1_state)
	{
		active = p1_state;
	}

	public boolean isActive()
	{
		return(active);
	}

	public void setRepeats()
	{
		setRepeats(true);
	}

	public boolean doesRepeat()
	{
		return(repeats);
	}

	public void setRepeats(boolean p1_bool)
	{
		repeats = p1_bool;
	}

	@Override
	public boolean readyToAdd()
	{
		boolean retval = super.readyToAdd();
		long now = Calendar.getInstance().getTime().getTime();
		retval = (retval && mepochTime >= now);
		retval = (retval && alarmTime > 0);
		Log.d(TAG,"schedule is "+((retval)?"":"not ")+"ready to add");
		return(retval);
	}

	@Override
	public String toString()
	{
		return((new StringBuilder())
			.append("id: " +id +", ")
			.append("alarmType: " +myType +", ")
			.append("active: " +(isActive()?"Yes":"No") +", ")
			.append("repeats: " +(doesRepeat()?"Yes":"No") +", ")
			.append("alarm time: " +getAlarmTime() +", ")
			.append("occurrence: " +occurrence +", ")
			.append("epoch: " +getEpochTime())
			.toString()
		);
	}
}
