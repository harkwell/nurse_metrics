package com.khallware.nurseMetrics;

public enum Severity
{
	NONE,          // 0
	MINOR,         // 1
	AVERAGE,       // 2
	MAJOR;         // 3
}
