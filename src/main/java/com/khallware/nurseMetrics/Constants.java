package com.khallware.nurseMetrics;

import android.graphics.Color;
import java.util.HashMap;
import java.lang.Integer;

public class Constants
{
	public static final String TAG_MAIN        = "nurseMetrics";
	public static final String TAG_ADDSEL      = "AddSelActivity";
	public static final String TAG_DIAPERACT   = "DiaperEventActivity";
	public static final String TAG_EVENTACT    = "AbstractEventActivity";
	public static final String TAG_SCHEDACT    = "ScheduleEventActivity";
	public static final String TAG_MEALACT     = "MealActivity";
	public static final String TAG_BOTTLEACT   = "BottleActivity";
	public static final String TAG_CONFIG      = "Config";
	public static final String TAG_ABSTEVENT   = "event";
	public static final String TAG_CHILD       = "Child";
	public static final String TAG_NAP         = "Nap";
	public static final String TAG_MEAL        = "Meal";
	public static final String TAG_SCHEDULE    = "Schedule";
	public static final String TAG_BOTTLE      = "Bottle";
	public static final String TAG_DIAPER      = "Diaper";
	public static final String TAG_EVNTFACTORY = "EventFactory";
	public static final String TAG_DBHELPER    = "OpenHelper";
	public static final String TAG_ALRMRCV     = "EventAlarmReceiver";
	public static final String TAG_S_CHART     = "SummaryChart";
	public static final String RESULT_COLUMN   = "retval";
	public static final String DBNAME          = "nurse_metrics";
	public static final String CONF_FILE       = "nurse_metrics.properties";
	public static final String CONF_DIR        = "/sdcard/";
	public static final String ALARM_MESSAGE   = "NurseMetrics Alarm";
	public static final String ERR_FILEREAD    = "failed to read file";
	public static final String ERR_INVALIDYR   = "invalid year";
	public static final String ERR_INVALIDMO   = "invalid month";
	public static final String ERR_INVALIDDAY  = "invalid day";
	public static final String ERR_INVALIDHR   = "invalid hour";
	public static final String ERR_INVALIDMIN  = "invalid minut";
	public static final String WARN_REQUIRED   = "user input required";
	public static final String WARN_INVALID    = "invalid user input";
	public static final String WARN_LARGENUM   = "number too large";
	public static final String WARN_SMALLNUM   = "number too small";
	public static final String EXTRA_CHILD     = "child";
	public static final String EXTRA_CHILDOBJ  = "child_object";
	public static final String EXTRA_CHLD_NAME = "child_name";
	public static final String EXTRA_DEST      = "destination";
	public static final String EXTRA_EVENTOBJ  = "event_object";
	public static final String EXTRA_ALARM     = "alarm";
	public static final String RPT_MAIN_TITLE  = "Stats For ";
	public static final String diaper_kstring  = "Diapers Per Day";
	public static final String nap_kstring     = "Nap Length/Day";
	public static final String milk_kstring    = "Ounces Per Day ";
	public static final boolean withTitle      = false;
	public static final int    CHART_DIAPERS   = 1;
	public static final int    CHART_NAPS      = 2;
	public static final int    CHART_BOTTLES   = 3;
	public static final int    MAX_SEV_LEVELS  = 10;
	public static final int    MIN_BOTTLE_VOL  = 0;
	public static final int    MAX_BOTTLE_VOL  = 100;
	public static final int    MIN_NAP_TIME    = 0;
	public static final int    MAX_NAP_TIME    = 1440;
	public static final int    MIN_MEAL_TIME   = 0;
	public static final int    MAX_MEAL_TIME   = 180;

	public static final HashMap<EventFactory.eventType, Integer> eventLayout
			= new HashMap<EventFactory.eventType, Integer>() {
		{
			this.put(EventFactory.eventType.DIAPER, 
					R.layout.diaper_event);
			this.put(EventFactory.eventType.NAP, 
					R.layout.nap_event);
			this.put(EventFactory.eventType.BOTTLE, 
					R.layout.bottle_event);
			this.put(EventFactory.eventType.MEAL, 
					R.layout.meal_event);
			this.put(EventFactory.eventType.SCHEDULE, 
					R.layout.schedule);
		}
	};
	public static final HashMap<EventFactory.eventType, Class> eventClass
			= new HashMap<EventFactory.eventType, Class>() {
		{
			this.put(EventFactory.eventType.DIAPER, 
					DiaperEventActivity.class);
			this.put(EventFactory.eventType.NAP, 
					NapEventActivity.class);
			this.put(EventFactory.eventType.BOTTLE, 
					BottleEventActivity.class);
			this.put(EventFactory.eventType.MEAL, 
					MealEventActivity.class);
			this.put(EventFactory.eventType.SCHEDULE, 
					ScheduleEventActivity.class);
		}
	};
	public static final HashMap<String, Integer> colors
				= new HashMap<String, Integer>() {
		{
			this.put("diapers_over_time", 
					Color.parseColor("#51748e"));
			this.put("naps_over_time", 
					Color.parseColor("#ff2423"));
			this.put("milk_over_time", 
					Color.parseColor("#fcff23"));
		}
	};
	public static final String[] charts = new String[] {
			"Diapers Per Day",
			"Nap Length/Day",
			"Ounces Per Day"
	};
}
