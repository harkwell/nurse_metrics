package com.khallware.nurseMetrics;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.Calendar;

public class Bottle extends AbstractEvent
{
	private static final String TAG = Constants.TAG_BOTTLE;
	public  static final int MAX_VOLUME = Constants.MAX_BOTTLE_VOL;
	public  static final int MIN_VOLUME = Constants.MIN_BOTTLE_VOL;

	private int ounces = 0;

	public Bottle()
	{
		Log.d(TAG, "Bottle() constructor");
	}

	private Bottle(Parcel p1_input)
	{
		super(p1_input);
		Log.d(TAG, "Bottle(Parcel) constructor");
		ounces = p1_input.readInt();
	}

	@Override
	public void writeToParcel(Parcel p1_out, int p2_flags)
	{
		super.writeToParcel(p1_out, p2_flags);
		p1_out.writeInt(ounces);
	}

	public static final Parcelable.Creator<Bottle> CREATOR = 
		new Parcelable.Creator<Bottle>() {
			public Bottle createFromParcel(Parcel p1_input) {
				return(new Bottle(p1_input));
			}
			public Bottle[] newArray(int p1_size) {
				return(new Bottle[p1_size]);
			}
		};

	public int getVolume()
	{
		return(ounces);
	}

	public void setVolume(int p1_volume)
	{
		if (p1_volume > 0) {
			ounces = p1_volume;
		}
	}

	@Override
	public boolean readyToAdd()
	{
		boolean retval = super.readyToAdd();
		long now = Calendar.getInstance().getTime().getTime();
		retval = (retval && ounces > 0);
		retval = (retval && mepochTime < now);
		Log.d(TAG, "bottle is " +((retval)?"":"not ") 
			+"ready to be added to the DB");
		return(retval);
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append("type: ").append(getType()).append(", ")
			.append("id: ").append("" +id).append(", ")
			.append("occurrence: ").append(occurrence).append(", ")
			.append("epoch: ").append("" +getEpochTime())
			.append(", ")
			.append("volume: ").append("" +ounces).append(" oz")
			.toString()
		);
	}

}
