package com.khallware.nurseMetrics;

import android.util.Log;
import android.view.*;
import android.widget.*;
import java.util.regex.Pattern;
import junit.framework.Assert;

public class BottleEventActivity extends AbstractEventActivity
{
	private final static String TAG = Constants.TAG_BOTTLEACT;
	private TextView volume = null;
	private ImageButton btn_plus = null;
	private ImageButton btn_minus = null;

	protected EventFactory.eventType getType()
	{
		return(EventFactory.eventType.BOTTLE);
	}

	protected void userPressedUpdate() { }
	protected void userPressedDelete() { }

	protected void setupView()
	{
		volume = (TextView)findViewById(R.id.volume);
		btn_plus = (ImageButton)findViewById(R.id.plus);
		btn_minus = (ImageButton)findViewById(R.id.minus);

		btn_plus.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				readFromView();
				int v = ((Bottle)getEventRef()).getVolume();
				((Bottle)getEventRef()).setVolume(++v);
				writeToView();
			}
		});
		btn_minus.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				readFromView();
				int v = ((Bottle)getEventRef()).getVolume();
				((Bottle)getEventRef()).setVolume(--v);
				writeToView();
			}
		});
	}

	protected void readFromView()
	{
		boolean flag = true;
		Log.d(TAG, "readFromView()ing...");
		Assert.assertTrue(getEventRef() != null);
		Assert.assertTrue(volume != null);

		String userInput = volume.getText().toString();

		if (!Pattern.matches("^\\d*$", userInput)) {
			warnMsg = Constants.WARN_INVALID;
		}
		else if (Integer.parseInt(userInput) < Bottle.MIN_VOLUME) {
			warnMsg = Constants.WARN_SMALLNUM;
		}
		else if (Integer.parseInt(userInput) > Bottle.MAX_VOLUME) {
			warnMsg = Constants.WARN_LARGENUM;
		}
		else {
			((Bottle)getEventRef()).setVolume(
				Integer.parseInt(userInput));
			flag = false;
		}
		if (flag) {
			Log.d(TAG, warnMsg);
			showDialog(WARN_DIALOG_ID);
		}
	}

	protected void writeToView()
	{
		Log.d(TAG, "writeToView()ing...");
		Assert.assertTrue(getEventRef() != null);
		Assert.assertTrue(volume != null);

		if (Constants.withTitle) {
			setTitle(R.string.bottle_act);
		}
		volume.setText("" +((Bottle)getEventRef()).getVolume());
	}
}
