package com.khallware.nurseMetrics;

import android.util.Log;
import junit.framework.Assert;

public class EventFactory
{
	private static final String TAG = Constants.TAG_EVNTFACTORY;

	public enum eventType
	{
		DIAPER, BOTTLE, MEAL, NAP, SCHEDULE
	}

	public static AbstractEvent createEvent(eventType p1_type)
	{
		Log.d(TAG, "creating "+p1_type+" event object");
		AbstractEvent retval = null;

		switch (p1_type) {
		case DIAPER:
			retval = new Diaper();
			break;
		case NAP:
			retval = new Nap();
			break;
		case BOTTLE:
			retval = new Bottle();
			break;
		case MEAL:
			retval = new Meal();
			break;
		case SCHEDULE:
			retval = new Schedule();
			break;
		default:
			Assert.assertTrue(false);
		}
		retval.setType(p1_type);
		return(retval);
	}
}
