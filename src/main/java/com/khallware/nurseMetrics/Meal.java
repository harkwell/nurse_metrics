package com.khallware.nurseMetrics;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.Calendar;
import junit.framework.Assert;

public class Meal extends AbstractEvent
{
	private static final String TAG = Constants.TAG_MEAL;
	public  static final int MIN_MINUTES = Constants.MIN_MEAL_TIME;
	public  static final int MAX_MINUTES = Constants.MAX_MEAL_TIME;

	private int leftSeconds = 0;
	private int rightSeconds = 0;
	private breast lastBreast = breast.UNKNOWN;

	public enum breast 
	{
		UNKNOWN, LEFT, RIGHT
	}

	public Meal()
	{
		Log.d(TAG, "constructing meal object");
	}

	private Meal(Parcel p1_input)
	{
		super(p1_input);
		Log.d(TAG, "constructing meal object from Parcel");
		leftSeconds = p1_input.readInt();
		rightSeconds = p1_input.readInt();
		lastBreast = breast.values()[p1_input.readInt()];
	}

	@Override
	public void writeToParcel(Parcel p1_out, int p2_flags)
	{
		super.writeToParcel(p1_out, p2_flags);
		p1_out.writeInt(leftSeconds);
		p1_out.writeInt(rightSeconds);
		p1_out.writeInt(lastBreast.ordinal());
	}

	public static final Parcelable.Creator<Meal> CREATOR = 
		new Parcelable.Creator<Meal>() {
			public Meal createFromParcel(Parcel p1_input) {
				return(new Meal(p1_input));
			}
			public Meal[] newArray(int p1_size) {
				return(new Meal[p1_size]);
			}
		};

	public int getLengthSeconds()
	{
		return(leftSeconds+rightSeconds);
	}

	public int getLeftSeconds()
	{
		return(leftSeconds);
	}

	public int getRightSeconds()
	{
		return(rightSeconds);
	}

	public void setLeftSeconds(int p1_sec)
	{
		Assert.assertTrue(p1_sec >= 0);

		leftSeconds = p1_sec;
	}

	public void setRightSeconds(int p1_sec)
	{
		Assert.assertTrue(p1_sec >= 0);

		rightSeconds = p1_sec;
	}

	public void setLastBreast(breast p1_breast)
	{
		lastBreast = p1_breast;
	}

	public boolean wasLastBreastRight()
	{
		return(lastBreast == breast.RIGHT);
	}

	public boolean wasLastBreastLeft()
	{
		return(lastBreast == breast.LEFT);
	}

	@Override
	public boolean readyToAdd()
	{
		boolean retval = super.readyToAdd();
		long now = Calendar.getInstance().getTime().getTime();
		retval = (retval && mepochTime < now);
		retval = (retval && leftSeconds+rightSeconds > 0);
		retval = (retval && lastBreast != breast.UNKNOWN);
		return(retval);
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append("type: " +getType() +", ")
			.append("id: " +id +", ")
			.append("occurrence: " +occurrence +", ")
			.append("epoch: " +getEpochTime() +", ")
			.append("last breast: " +lastBreast +", ")
			.append("left timec: " +leftSeconds +", ")
			.append("right time: " +rightSeconds)
			.toString()
		);
	}
}
