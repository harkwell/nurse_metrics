package com.khallware.nurseMetrics;

import android.util.Log;
import android.os.*;
import android.widget.*;
import android.content.*;
import android.media.MediaPlayer;
import android.media.AudioManager;
import java.text.SimpleDateFormat;
import java.util.Date;
import junit.framework.Assert;

public class EventAlarmReceiver extends BroadcastReceiver
{
	private static final String TAG = Constants.TAG_ALRMRCV;
	private String note = Constants.ALARM_MESSAGE;
	private Schedule sched = null;
	private Child child = null;
	private MediaPlayer player = null;

	@Override
	public void onReceive(Context p1_ctx, Intent p2_i)
	{
		Log.d(TAG, "onReceive()ing...at: "
			+(new SimpleDateFormat("MM/dd/yyyy hh:mm:ss"))
			.format(new Date()));
		sched = (Schedule)p2_i.getParcelableExtra(
			Constants.EXTRA_ALARM);
		child = OpenHelper.getHelper(p1_ctx).getChild(sched.childId);
		Schedule.alarmType type = sched.getAlarmType();
		Log.d(TAG, "alarming alarm, " +sched.toString());
		try {
			note += ": " +child.name +" " +type +" alarm.";
			Toast.makeText(p1_ctx, note, Toast.LENGTH_LONG).show();

			switch (type) {
			case GENERIC:
				player = MediaPlayer.create(
						p1_ctx,
						R.raw.alarm);
				break;
			case WAKE:
			case SLEEP:
				player = MediaPlayer.create(
						p1_ctx,
						R.raw.sleep_alarm);
				break;
			case FEED:
				player = MediaPlayer.create(
						p1_ctx,
						R.raw.eat_alarm);
				break;
			case MEDICAL:
				player = MediaPlayer.create(
						p1_ctx,
						R.raw.rx_alarm);
				break;
			case BATH:
				player = MediaPlayer.create(
						p1_ctx,
						R.raw.bath_alarm);
				break;
			default:
				Assert.assertTrue(false);
			}
			AudioManager audioManager = (AudioManager)
				p1_ctx.getSystemService(Context.AUDIO_SERVICE);

			if (audioManager.getRingerMode() != 
					AudioManager.RINGER_MODE_SILENT) {
				player.start();
			}
			else {
				Log.w(TAG, "not playing audio, phone muted");
			}
			Log.i(TAG, note);
		}
		catch (Throwable e) {
			Toast.makeText(
				p1_ctx,
				Constants.ALARM_MESSAGE,
				Toast.LENGTH_LONG)
			.show();
		}
	}
}
