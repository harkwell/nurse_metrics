package com.khallware.nurseMetrics;

import android.util.Log;
import android.widget.*;
import android.view.*;
import java.util.regex.Pattern;
import junit.framework.Assert;

public class NapEventActivity extends AbstractEventActivity
{
	private final static String TAG = Constants.TAG_DIAPERACT;
	private TextView hour = null;
	private TextView min = null;
	private ImageButton hr_plus = null;
	private ImageButton hr_minus = null;
	private ImageButton min_plus = null;
	private ImageButton min_minus = null;

	protected EventFactory.eventType getType()
	{
		return(EventFactory.eventType.NAP);
	}

	protected void userPressedUpdate() { }
	protected void userPressedDelete() { }

	protected void setupView()
	{
		hour = (TextView)findViewById(R.id.nap_len_hr);
		min = (TextView)findViewById(R.id.nap_len_min);
		hr_plus = (ImageButton)findViewById(R.id.hrplus);
		hr_minus = (ImageButton)findViewById(R.id.hrminus);
		min_plus = (ImageButton)findViewById(R.id.minplus);
		min_minus = (ImageButton)findViewById(R.id.minminus);

		hr_plus.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				readFromView();
				int len = ((Nap)getEventRef()).getLength();
				((Nap)getEventRef()).setLength(len+60);
				writeToView();
			}
		});
		hr_minus.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				readFromView();
				int len = ((Nap)getEventRef()).getLength();

				if (len > 60) {
					((Nap)getEventRef()).setLength(len-60);
					writeToView();
				}
				else if (len == 60) {
					((Nap)getEventRef()).setLength(1);
					writeToView();
				}
			}
		});
		min_plus.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				readFromView();
				int len = ((Nap)getEventRef()).getLength();
				((Nap)getEventRef()).setLength(++len);
				writeToView();
			}
		});
		min_minus.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				readFromView();
				int len = ((Nap)getEventRef()).getLength();

				if (len > 0) {
					((Nap)getEventRef()).setLength(--len);
					writeToView();
				}
			}
		});
	}

	protected void writeToView()
	{
		Log.d(TAG, "writeToView()ing...");
		Assert.assertTrue(getEventRef() != null);
		Assert.assertTrue(hour != null);
		Assert.assertTrue(min != null);

		if (Constants.withTitle) {
			setTitle(R.string.nap_act);
		}
		hour.setText("" +(int)((Nap)getEventRef()).getLength()/60);
		min.setText("" +((Nap)getEventRef()).getLength()%60);
	}

	protected void readFromView()
	{
		Log.d(TAG, "readFromView()ing...");
		Assert.assertTrue(hour != null);
		Assert.assertTrue(min != null);

		String hourInput = hour.getText().toString();
		String minInput = min.getText().toString();
		boolean flag = true;

		if (hourInput.compareTo("")==0 && minInput.compareTo("")==0) {
			Log.d(TAG, "no user input yet");
			flag = false;
		}
		else if (hourInput.compareTo("") == 0 
				&& !Pattern.matches("^\\d*$", minInput)) {
			warnMsg = Constants.WARN_INVALID;
		}
		else if (minInput.compareTo("") == 0
				&& !Pattern.matches("^\\d*$", hourInput)) {
			warnMsg = Constants.WARN_INVALID;
		}
		else if (!Pattern.matches("^\\d*$", hourInput)
				|| !Pattern.matches("^\\d*$", minInput)) {
			warnMsg = Constants.WARN_INVALID;
		}
		else {
			int length = Integer.parseInt(hourInput) * 60
				+ Integer.parseInt(minInput);

			if (length < Nap.MIN_TIME) {
				warnMsg = Constants.WARN_SMALLNUM;
			}
			else if (length > Nap.MAX_TIME) {
				warnMsg = Constants.WARN_LARGENUM;
			}
			else {
				((Nap)getEventRef()).setLength(length);
				flag = false;
			}
		}
		if (flag) {
			Log.w(TAG, warnMsg);
			showDialog(WARN_DIALOG_ID);
		}
	}
}
