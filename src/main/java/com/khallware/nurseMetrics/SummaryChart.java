package com.khallware.nurseMetrics;

import android.util.Log;
import android.content.Intent;
import android.content.Context;
import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;

public class SummaryChart
{
	private static final String TAG = Constants.TAG_S_CHART;
	private static SummaryChart mySummaryChart = null;
	private static XYMultipleSeriesDataset dataset = null;

	private SummaryChart() { }

	public static synchronized Intent render(Context p1_ctx, Child p2_child,
			int p3_chart)
	{
		Log.d(TAG, "render()ing...graph for child: " +p2_child.name);
		Intent retval = null;

		if (mySummaryChart == null) {
			mySummaryChart = new SummaryChart();
		}
		dataset = OpenHelper.getHelper(p1_ctx).getSummaryDataset(
			p2_child, p3_chart);
		retval = ChartFactory.getTimeChartIntent(p1_ctx, dataset,
				mySummaryChart.getRenderer(), "MM/dd");
		return(retval);
	}

	private XYMultipleSeriesRenderer getRenderer()
	{
		XYMultipleSeriesRenderer retval=new XYMultipleSeriesRenderer();
		XYSeriesRenderer tmp = null;
		tmp = new XYSeriesRenderer();
		tmp.setColor(Constants.colors.get("diapers_over_time"));
		tmp.setPointStyle(PointStyle.CIRCLE);
		retval.addSeriesRenderer(tmp);
		return(retval);
	}
}
