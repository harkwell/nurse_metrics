package com.khallware.nurseMetrics;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.database.SQLException;
import android.content.SharedPreferences;
import android.util.Log;
import org.achartengine.model.XYSeries;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import junit.framework.Assert;
import java.util.Date;
import java.text.SimpleDateFormat;

public class OpenHelper extends SQLiteOpenHelper
{
	private static final String TAG = Constants.TAG_DBHELPER;
	private static final String DEF_DBNAME = Constants.DBNAME;
	private static OpenHelper helper = null;

	private SQLiteDatabase dbh = null;
	private static Config config = Config.getConfig();

	private OpenHelper(Context p1_ctx, String p2_dbname, int p3_dbver)
	{
		super((Context)p1_ctx, p2_dbname, null, p3_dbver);
		Log.d(TAG, "constructing openHelper object");
		grab_handle();
	}

	private void grab_handle()
	{
		if (dbh == null || !dbh.isOpen()) {
			dbh = getWritableDatabase();
		}
	}

	public static synchronized OpenHelper getHelper(Context p1_ctx)
	{
		if (helper == null) {
			Assert.assertTrue(
				config.get("db_name").compareTo("") != 0);
			Assert.assertTrue(
				config.get("db_ver").compareTo("") != 0);
			helper = new OpenHelper((Context)
					p1_ctx, 
					config.get("db_name"), 
					Integer.parseInt(config.get("db_ver"))
				);
		}
		return(helper);
	}

	@Override
	public void onCreate(SQLiteDatabase p1_db)
	{
		Log.d(TAG, "onCreate()ing...");
		dbh = p1_db;
		createDB();
	}

	@Override
	public void onUpgrade(SQLiteDatabase p1_db, int p2_old_ver, 
		int p3_new_ver)
	{
		Log.d(TAG, "onUpgrade()ing...");
		Log.d(TAG, "no db/table changes required");
	}

	public String[] getSeverityLevels()
	{
		Log.d(TAG, "getSeverityLevels()ing...");
		grab_handle();
		String[] retval = null;
		String sql = "SELECT name "
			+      "FROM severity "
			+  "ORDER BY _id "
			+     "LIMIT " +Constants.MAX_SEV_LEVELS;
		Log.d(TAG, "sql: " +sql);
		Cursor c = dbh.rawQuery(sql, new String[] {});
		retval = new String[c.getCount()];
		
		for (int i=0; c.moveToNext(); i++) {
			retval[i] = c.getString(c.getColumnIndex("name"));
		}
		return(retval);
	}

	public Cursor queryChildren()
	{
		Log.d(TAG, "queryChildren()ing...");
		grab_handle();
		String sql = "SELECT _id, name "
			+        "AS " +Constants.RESULT_COLUMN +" "
			+      "FROM child "
			+  "ORDER BY name";
		Log.d(TAG, "sql: " +sql);
		return(dbh.rawQuery(sql, new String[] {}));
	}

	public Cursor queryAlarms()
	{
		Log.d(TAG, "queryAlarms()ing...");
		grab_handle();
		String sql = "SELECT scheduled_event._id, ("
			+                 "strftime('%m-%d %H:%M', occurrence,"
			+                          "'unixepoch', 'localtime') "
			+                 "|| ' ' "
			+                 "|| scheduled_event_type.name || ' ' "
			+                 "|| child.name || ' ' "
			+                 "|| (SELECT CASE active "
			+                            "WHEN 1 THEN 'Active' "
			+                            "ELSE 'Inactive' "
			+                            "END"
			+                     ") "
			+                 "|| ' ' "
			+                 "|| (SELECT CASE repeat "
			+                            "WHEN 1 THEN 'Repeat' "
			+                            "ELSE 'No Repeat' "
			+                            "END"
			+                     ")"
			+            ") "
			+        "AS " +Constants.RESULT_COLUMN +" "
			+      "FROM scheduled_event "
			+ "LEFT JOIN child "
			+        "ON child._id = scheduled_event.child "
			+ "LEFT JOIN scheduled_event_type "
			+        "ON scheduled_event_type._id = "
			+                                "scheduled_event.type "
			+     "WHERE active > 0 "
			+        "OR repeat > 0 "
			+  "ORDER BY child.name, repeat";
		Log.d(TAG, "sql: " +sql);
		return(dbh.rawQuery(sql, new String[] {}));
	}

	public void cleanAlarms()
	{
		Log.d(TAG, "cleanAlarms()ing...");
		grab_handle();
		String sql = "UPDATE scheduled_event "
			+       "SET active = 0 "
			+     "WHERE active > 0 "
			+       "AND repeat < 1 "
			+       "AND occurrence < "
			+                     "strftime('%s', datetime('now'))";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { });
	}

	public Child getChild(long p1_id)
	{
		Log.d(TAG, "getChild(" +p1_id +")ing...");
		grab_handle();
		Child retval = new Child();
		retval.id = 0;
		String sql = "SELECT _id, name, gender "
			+      "FROM child "
			+     "WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		Cursor c = dbh.rawQuery(sql, new String[] { "" +p1_id });
		
		if (c.moveToFirst()) {
			retval.id = p1_id;
			retval.name = c.getString(c.getColumnIndex("name"));
			retval.setGirl();
			int boyOrd = Child.gender.BOY.ordinal();

			if (c.getInt(c.getColumnIndex("gender")) == boyOrd) {
				retval.setBoy();
			}
		}
		Log.d(TAG, "got child " +retval.toString());
		return(retval);
	}

	public Child getChildByName(String p1_name)
	{
		Log.d(TAG, "getChildByName(" +p1_name +")ing...");
		grab_handle();
		Child retval = new Child();
		retval.name = p1_name;
		String sql = "SELECT _id, name, gender "
			+      "FROM child "
			+     "WHERE name = ?";
		Log.d(TAG, "sql: " +sql);
		Cursor c = dbh.rawQuery(sql, new String[] { "" +p1_name });
		
		if (c.moveToFirst()) {
			retval.id = c.getInt(c.getColumnIndex("_id"));
			retval.setGirl();

			if (c.getInt(c.getColumnIndex("gender")) 
					== Child.gender.BOY.ordinal()) {
				retval.setBoy();
			}
		}
		Log.d(TAG, "got child " +retval.toString());
		return(retval);
	}

	public void addChild(Child p1_child)
	{
		String name = p1_child.name;
		grab_handle();
		int gender = (p1_child.isBoy()) 
				? Child.gender.BOY.ordinal()
				: Child.gender.GIRL.ordinal();
		Log.d(TAG, "addChild(" +p1_child.toString() +")ing...");
		Assert.assertTrue(p1_child.readyToAdd());
		Assert.assertFalse(p1_child.updateable());

		String sql = "INSERT INTO child VALUES (NULL, ?, ?)";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { "" +gender, name });
	}

	public void updateChild(Child p1_child)
	{
		long id = p1_child.id;
		grab_handle();
		String name = p1_child.name;
		Log.d(TAG, "updateChild(" +id +", \"" +name +"\")ing...");
		Assert.assertTrue(p1_child.updateable());
		String sql = "UPDATE child "
			+       "SET name = ?, "
			+           "gender = ? "
			+     "WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { 
			name, "" +p1_child.sex.ordinal(), "" +id
		});
	}

	public Cursor queryEvent(AbstractEvent p1_event)
	{
		Log.d(TAG, "queryEvent(" +p1_event.toString() +")ing...");
		Cursor retval = null;
		Assert.assertTrue(p1_event != null);

		if (p1_event instanceof com.khallware.nurseMetrics.Diaper) {
			retval = queryDiapers(p1_event.childId);
		}
		else if (p1_event instanceof com.khallware.nurseMetrics.Nap) {
			retval = queryNaps(p1_event.childId);
		}
		else if (p1_event instanceof com.khallware.nurseMetrics.Bottle){
			retval = queryBottles(p1_event.childId);
		}
		else if (p1_event instanceof com.khallware.nurseMetrics.Meal) {
			retval = queryMeals(p1_event.childId);
		}
		else if (p1_event instanceof 
				com.khallware.nurseMetrics.Schedule) {
			retval = queryAlarms();
		}
		else {
			Assert.assertTrue(false);
		}
		Log.d(TAG, "got db cursor listing type " +p1_event.getType());
		return(retval);
	}

	public Cursor queryDiapers(long p1_child)
	{
		Log.d(TAG, "queryDiapers()ing...");
		grab_handle();
		String sql = "SELECT _id, (strftime('%m-%d %H:%M', occurrence, "
			+                          "'unixepoch', 'localtime')"
			+                 " || ' '"
			+                 " || (SELECT severity.name "
			+                        "FROM severity "
			+                       "WHERE severity._id = poopy) "
			+                 " || ' dirty ' "
			+                 " || (SELECT severity.name "
			+                        "FROM severity "
			+                       "WHERE severity._id = wet) "
			+                 " || ' wet'"
			+            ") "
			+        "AS " +Constants.RESULT_COLUMN +" "
			+      "FROM diaper_event "
			+     "WHERE child = ? "
			+  "ORDER BY occurrence DESC";
		Log.d(TAG, "sql: " +sql);
		return(dbh.rawQuery(sql, new String[] { "" +p1_child }));
	}

	public Cursor queryNaps(long p1_child)
	{
		Log.d(TAG, "queryNaps()ing...");
		grab_handle();
		String sql = "SELECT _id, "
			+            "('start: ' "
			+              "|| strftime('%m-%d %H:%M', ("
			+                      "SELECT datetime(begin, "
			+                           "'unixepoch', 'localtime'))"
			+                  ") "
			+              "|| ' length (hr:min): ' "
			+              "|| (SELECT (naplen/60)) || ':'"
			+              "|| (SELECT CASE length("
			+                               "(''||(naplen%60))) "
			+                         "WHEN 1 THEN substr("
			+                               "('0'||(naplen%60)),0) "
			+                         "ELSE "
			+                               "(naplen%60) "
			+                         "END"
			+                 ")"
			+            ") "
			+        "AS " +Constants.RESULT_COLUMN +" "
			+      "FROM nap_event "
			+     "WHERE child = ? "
			+  "ORDER BY begin DESC";
		Log.d(TAG, "sql: " +sql);
		return(dbh.rawQuery(sql, new String[] { "" +p1_child }));
	}

	public Cursor queryBottles(long p1_child)
	{
		Log.d(TAG, "queryBottles()ing...");
		grab_handle();
		String sql = "SELECT bottle_event._id, ('began: ' "
			+                 "|| strftime('%m-%d %H:%M', ("
			+                         "SELECT datetime(begin, "
			+                           "'unixepoch', 'localtime'))"
			+                    ") "
			+                 "|| ', volume: ' "
			+                 "|| ounces || ' oz'"
			+            ") "
			+        "AS " +Constants.RESULT_COLUMN +" "
			+      "FROM bottle_event "
			+ "LEFT JOIN child "
			+        "ON child._id = bottle_event.child "
			+     "WHERE child = ? "
			+  "ORDER BY begin DESC";
		Log.d(TAG, "sql: " +sql);
		return(dbh.rawQuery(sql, new String[] { "" +p1_child }));
	}

	public Cursor queryMeals(long p1_child)
	{
		Log.d(TAG, "queryMeals()ing...");
		grab_handle();
		String sql = "SELECT meal_event._id, ("
			+                 "strftime('%m-%d %H:%M', ("
			+                         "SELECT datetime(begin, "
			+                           "'unixepoch', 'localtime'))"
			+                     ") "
			+                 "|| ', length (hr:min): ' "
			+                 "|| ((left_secs + right_secs)/60)"
			+                 "|| ':' "
			+                 "|| (SELECT CASE length("
			+                "(''||((left_secs + right_secs)%60))) "
			+                            "WHEN 1 THEN substr("
			+             "('0'||((left_secs + right_secs)%60)),0) "
			+                            "ELSE substr("
			+                  "((left_secs + right_secs)%60),0,2) "
			+                            "END"
			+                    ")"
			+                 "|| ', ' || 'last: ' "
			+                 "|| (SELECT CASE last_breast "
			+                            "WHEN 1 THEN 'RIGHT' "
			+                            "ELSE 'LEFT' "
			+                            "END"
			+                     ")"
			+            ") "
			+        "AS " +Constants.RESULT_COLUMN +" "
			+      "FROM meal_event "
			+ "LEFT JOIN child "
			+        "ON child._id = meal_event.child "
			+     "WHERE child = ? "
			+  "ORDER BY begin DESC";
		Log.d(TAG, "sql: " +sql);
		return(dbh.rawQuery(sql, new String[] { "" +p1_child }));
	}

	public AbstractEvent getEvent(long p1_id, EventFactory.eventType p2_t)
	{
		Log.d(TAG, "getEvent(" +p1_id +", " +p2_t +")ing...");
		AbstractEvent retval = null;
		Assert.assertTrue(p1_id > 0);
		Assert.assertTrue(p2_t != null);

		if (p2_t == EventFactory.eventType.DIAPER) {
			retval = getDiaper(p1_id);
		}
		else if (p2_t == EventFactory.eventType.BOTTLE) {
			retval = getBottle(p1_id);
		}
		else if (p2_t == EventFactory.eventType.MEAL) {
			retval = getMeal(p1_id);
		}
		else if (p2_t == EventFactory.eventType.NAP) {
			retval = getNap(p1_id);
		}
		else if (p2_t == EventFactory.eventType.SCHEDULE) {
			retval = getAlarm(p1_id);
		}
		else {
			Assert.assertTrue(false);
		}
		Log.d(TAG,"got "+retval.getType()+": ("+retval.toString()+")");
		return(retval);
	}

	public void deleteEvent(AbstractEvent p1_event)
	{
		Log.d(TAG, "deleteEvent(" +p1_event +")ing...");
		Assert.assertTrue(p1_event != null && p1_event.id > 0);

		EventFactory.eventType type = p1_event.getType();

		if (type == EventFactory.eventType.DIAPER) {
			deleteDiaper(p1_event.id);
		}
		else if (type == EventFactory.eventType.BOTTLE) {
			deleteBottle(p1_event.id);
		}
		else if (type == EventFactory.eventType.MEAL) {
			deleteMeal(p1_event.id);
		}
		else if (type == EventFactory.eventType.NAP) {
			deleteNap(p1_event.id);
		}
		else if (type == EventFactory.eventType.SCHEDULE) {
			deleteAlarm(p1_event.id);
		}
		else {
			Assert.assertTrue(false);
		}
	}

	public boolean saveEvent(AbstractEvent p1_event)
	{
		Log.d(TAG, "saveEvent(" +p1_event.toString() +")ing...");
		boolean retval = true;

		if (!p1_event.readyToAdd()) {
			retval = false;
		}
		else if (p1_event instanceof Diaper) {
			Log.d(TAG, "found a diaper");

			if (((Diaper)p1_event).updateable()) {
				updateDiaper((Diaper)p1_event);
			}
			else if (((Diaper)p1_event).readyToAdd()) {
				Diaper tmp = getDiaperByEpoch(
					((Diaper)p1_event).getEpochTime()
				);
				if (tmp == null) {
					addDiaper((Diaper)p1_event);
				}
			}
			else {
				Log.e(TAG, "invalid diaper event");
				retval = false;
			}
		}
		else if (p1_event instanceof Nap) {
			Log.d(TAG, "found a nap");

			if (((Nap)p1_event).updateable()) {
				updateNap((Nap)p1_event);
			}
			else if (((Nap)p1_event).readyToAdd()) {
				addNap((Nap)p1_event);
			}
			else {
				Log.e(TAG, "invalid nap event");
				retval = false;
			}
		}
		else if (p1_event instanceof Bottle) {
			Log.d(TAG, "found a bottle");

			if (((Bottle)p1_event).updateable()) {
				updateBottle((Bottle)p1_event);
			}
			else if (((Bottle)p1_event).readyToAdd()) {
				addBottle((Bottle)p1_event);
			}
			else {
				Log.e(TAG, "invalid bottle event");
				retval = false;
			}
		}
		else if (p1_event instanceof Meal) {
			Log.d(TAG, "found a meal");

			if (((Meal)p1_event).updateable()) {
				updateMeal((Meal)p1_event);
			}
			else if (((Meal)p1_event).readyToAdd()) {
				addMeal((Meal)p1_event);
			}
			else {
				Log.e(TAG, "invalid meal event");
				retval = false;
			}
		}
		else if (p1_event instanceof Schedule) {
			Log.d(TAG, "found a schedule");

			if (((Schedule)p1_event).updateable()) {
				updateAlarm((Schedule)p1_event);
			}
			else if (((Schedule)p1_event).readyToAdd()) {
				addAlarm((Schedule)p1_event);
			}
			else {
				Log.e(TAG, "invalid schedule event");
				retval = false;
			}
		}
		else {
			Assert.assertTrue(false); // : "unknown event type";
			retval = false;
		}
		return(retval);
	}

	public Diaper getDiaperByEpoch(long p1_time)
	{
		Log.d(TAG, "getDiaper(" +p1_time +")ing...");
		grab_handle();
		Diaper retval = null;
		String sql = "SELECT _id FROM diaper_event WHERE occurrence=?";
		Log.d(TAG, "sql: " +sql);
		Cursor c = dbh.rawQuery(sql, new String[] { "" +p1_time });
		
		if (c.moveToFirst()) {
			retval = getDiaper(c.getInt(c.getColumnIndex("_id")));
			Log.d(TAG, "got diaper " +retval.toString());
		}
		return(retval);
	}

	public Diaper getDiaper(long p1_id)
	{
		Log.d(TAG, "getDiaper(" +p1_id +")ing...");
		grab_handle();
		Diaper retval = (Diaper)EventFactory.createEvent(
					EventFactory.eventType.DIAPER);
		retval.id = p1_id;
		String sql =
			"SELECT _id, child, occurrence, poopy, wet "
			+ "FROM diaper_event "
			+"WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		Cursor c = dbh.rawQuery(sql, new String[] { "" +p1_id });
		
		if (c.moveToFirst()) {
			retval.childId = c.getInt(c.getColumnIndex("child"));
			retval.setEpochTime(c.getLong(
				c.getColumnIndex("occurrence")));

			for (Severity s : Severity.values()) {
				if (c.getInt(c.getColumnIndex("poopy")) 
						== s.ordinal()) {
					retval.poopy = s;
				}
				if (c.getInt(c.getColumnIndex("wet")) 
						== s.ordinal()) {
					retval.wet = s;
				}
			}
		}
		Log.d(TAG, "got diaper " +retval.toString());
		return(retval);
	}

	public void addDiaper(Diaper p1_d)
	{
		Log.d(TAG, "addDiaper()ing...");
		Log.d(TAG, "diaper = " +p1_d.toString());
		grab_handle();

		String sql = "INSERT INTO diaper_event VALUES ("
			+       "NULL, ?, ?, ?, ?)";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] {
				"" +p1_d.childId,
				"" +p1_d.getEpochTime(), 
				"" +p1_d.poopy.ordinal(), 
				"" +p1_d.wet.ordinal()
			}
		);
	}

	public void updateDiaper(Diaper p1_d)
	{
		Log.d(TAG, "updateDiaper()ing...");
		Log.d(TAG, "diaper = " +p1_d.toString());
		grab_handle();

		String sql = "UPDATE diaper_event "
			+       "SET occurrence = ?, "
			+           "poopy = ?, "
			+           "wet = ? "
			+     "WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { 
				"" +p1_d.getEpochTime(), 
				"" +p1_d.poopy.ordinal(), 
				"" +p1_d.wet.ordinal(),
				"" +p1_d.id 
			}
		);
	}

	public void deleteChild(long p1_id)
	{
		Log.d(TAG, "deleteChild(" +p1_id +")ing...");
		grab_handle();
		String sql = "DELETE FROM child WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { "" +p1_id });

		for (String tab : new String[] {
				"diaper_event", "bottle_event", "nap_event",
				"meal_event", "scheduled_event" }) {
			sql = "DELETE FROM " +tab +" WHERE child = ?";
			Log.d(TAG, "sql: " +sql);
			dbh.execSQL(sql, new String[] { "" +p1_id });
		}
	}

	public void deleteDiaper(long p1_id)
	{
		Log.d(TAG, "deleteDiaper(" +p1_id +")ing...");
		grab_handle();
		String sql = "DELETE FROM diaper_event WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { "" +p1_id });
	}

	public Nap getNap(long p1_id)
	{
		Log.d(TAG, "getNap(" +p1_id +")ing...");
		grab_handle();
		Nap retval = (Nap)EventFactory.createEvent(
					EventFactory.eventType.NAP);
		retval.id = p1_id;
		String sql = 
			"SELECT _id, child, begin, naplen "
			+ "FROM nap_event "
			+"WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		Cursor c = dbh.rawQuery(sql, new String[] { "" +p1_id });
		
		if (c.moveToFirst()) {
			retval.childId = c.getInt(c.getColumnIndex("child"));
			retval.setEpochTime(c.getLong(
				c.getColumnIndex("begin")));
			retval.setLength(c.getInt(c.getColumnIndex("naplen")));
		}
		Log.d(TAG, "got nap " +retval.toString());
		return(retval);
	}

	public void addNap(Nap p1_nap)
	{
		Log.d(TAG, "addNap()ing...");
		Log.d(TAG, "nap: " +p1_nap.toString());
		grab_handle();

		String sql = "INSERT INTO nap_event VALUES ("
			+       "NULL, ?, ?, ?)";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] {
				"" +p1_nap.childId,
				"" +p1_nap.getEpochTime(), 
				"" +p1_nap.getLength()
			}
		);
	}

	public void updateNap(Nap p1_nap)
	{
		Log.d(TAG, "updateNap()ing...");
		Log.d(TAG, "nap: " +p1_nap.toString());
		grab_handle();

		String sql = "UPDATE nap_event "
			+       "SET child = ?, "
			+           "begin = ?, "
			+           "naplen = ? "
			+     "WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] {
				"" +p1_nap.childId,
				"" +p1_nap.getEpochTime(), 
				"" +p1_nap.getLength(), 
				"" +p1_nap.id 
			}
		);
	}

	public void deleteNap(long p1_id)
	{
		Log.d(TAG, "deleteNap(" +p1_id +")ing...");
		grab_handle();
		String sql = "DELETE FROM nap_event WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { "" +p1_id });
	}

	public Bottle getBottle(long p1_id)
	{
		Log.d(TAG, "getBottle(" +p1_id +")ing...");
		grab_handle();
		Bottle retval = (Bottle)EventFactory.createEvent(
					EventFactory.eventType.BOTTLE);
		retval.id = p1_id;
		String sql = "SELECT _id, child, begin, ounces "
			+      "FROM bottle_event "
			+     "WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		Cursor c = dbh.rawQuery(sql, new String[] { "" +p1_id });
		
		if (c.moveToFirst()) {
			retval.childId = c.getInt(c.getColumnIndex("child"));
			retval.setEpochTime(c.getLong(
				c.getColumnIndex("begin")));
			retval.setVolume(c.getInt(c.getColumnIndex("ounces")));
		}
		Log.d(TAG, "got bottle " +retval.toString());
		return(retval);
	}

	public void addBottle(Bottle p1_bottle)
	{
		Log.d(TAG, "addBottle()ing...");
		Log.d(TAG, "bottle: " +p1_bottle.toString());
		grab_handle();

		String sql = "INSERT INTO bottle_event VALUES ("
			+       "NULL, ?, ?, ?)";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { 
				"" +p1_bottle.childId,
				"" +p1_bottle.getEpochTime(), 
				"" +p1_bottle.getVolume()
			}
		);
	}

	public void updateBottle(Bottle p1_bottle)
	{
		Log.d(TAG, "updateBottle()ing...");
		Log.d(TAG, "bottle: " +p1_bottle.toString());
		grab_handle();

		String sql = "UPDATE bottle_event "
			+       "SET child = ?, "
			+           "begin = ?, "
			+           "ounces = ? "
			+     "WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] {
				"" +p1_bottle.childId,
				"" +p1_bottle.getEpochTime(), 
				"" +p1_bottle.getVolume(),
				"" +p1_bottle.id 
			}
		);
	}

	public void deleteBottle(long p1_id)
	{
		Log.d(TAG, "deleteBottle(" +p1_id +")ing...");
		grab_handle();
		String sql = "DELETE FROM bottle_event WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { "" +p1_id });
	}

	public Meal getMeal(long p1_id)
	{
		Log.d(TAG, "getMeal(" +p1_id +")ing...");
		grab_handle();
		Meal retval = (Meal)EventFactory.createEvent(
					EventFactory.eventType.MEAL);
		retval.id = p1_id;
		String sql = "SELECT _id, child, begin, left_secs, right_secs, "
			+           "last_breast "
			+      "FROM meal_event "
			+     "WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		Cursor c = dbh.rawQuery(sql, new String[] { "" +p1_id });
		
		if (c.moveToFirst()) {
			retval.childId = c.getInt(c.getColumnIndex("child"));
			retval.setEpochTime(c.getLong(
				c.getColumnIndex("begin")));
			retval.setLeftSeconds(c.getInt(c.getColumnIndex(
				"left_secs")));
			retval.setRightSeconds(c.getInt(c.getColumnIndex(
				"right_secs")));
			int lb_val = c.getInt(c.getColumnIndex("last_breast"));
			retval.setLastBreast((lb_val > 0)
					? Meal.breast.RIGHT
					: Meal.breast.LEFT
			);
		}
		Log.d(TAG, "got meal " +retval.toString());
		return(retval);
	}

	public void addMeal(Meal p1_meal)
	{
		Log.d(TAG, "addMeal()ing...");
		Log.d(TAG, "meal: " +p1_meal.toString());
		grab_handle();

		String sql = "INSERT INTO meal_event VALUES ("
			+       "NULL, ?, ?, ?, ?, ?)";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] {
				"" +p1_meal.childId,
				"" +p1_meal.getEpochTime(), 
				"" +p1_meal.getLeftSeconds(),
				"" +p1_meal.getRightSeconds(),
				"" +((p1_meal.wasLastBreastRight()) ? 1 : 0)
			}
		);
	}

	public void updateMeal(Meal p1_meal)
	{
		Log.d(TAG, "updateMeal()ing...");
		Log.d(TAG, "meal: " +p1_meal.toString());
		grab_handle();

		String sql = "UPDATE meal_event "
			+       "SET child = ?, "
			+           "begin = ?, "
			+           "left_secs = ?, "
			+           "right_secs = ?, "
			+           "last_breast = ? "
			+     "WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] {
				"" +p1_meal.childId,
				"" +p1_meal.getEpochTime(), 
				"" +p1_meal.getLeftSeconds(),
				"" +p1_meal.getRightSeconds(),
				"" +p1_meal.id,
				"" +((p1_meal.wasLastBreastRight()) ? 1 : 0)
			}
		);
	}

	public void deleteMeal(long p1_id)
	{
		Log.d(TAG, "deleteMeal(" +p1_id +")ing...");
		grab_handle();
		String sql = "DELETE FROM meal_event WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { "" +p1_id });
	}

	public Schedule getAlarm(long p1_id)
	{
		Log.d(TAG, "getAlarm(" +p1_id +")ing...");
		grab_handle();
		Schedule retval = (Schedule)EventFactory.createEvent(
					EventFactory.eventType.SCHEDULE);
		retval.id = p1_id;
		String sql = "SELECT _id, child, type, active, repeat, "
			+           "strftime('%s', occurrence, 'unixepoch') "
			+        "AS epo "
			+      "FROM scheduled_event "
			+     "WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		Cursor c = dbh.rawQuery(sql, new String[] { "" +p1_id });
		
		if (c.moveToFirst()) {
			retval.childId = c.getInt(c.getColumnIndex("child"));
			retval.setAlarmTime(c.getLong(c.getColumnIndex("epo")));
			retval.setActive(
				(c.getInt(c.getColumnIndex("active")) > 0));
			retval.setRepeats(
				(c.getInt(c.getColumnIndex("repeat")) > 0));
			int alarmTypeOrd = c.getInt(c.getColumnIndex("type"));
			retval.setAlarmType(
				Schedule.alarmType.values()[alarmTypeOrd]
			);
		}
		Log.d(TAG, "got alarm " +retval.toString());
		return(retval);
	}

	public void addAlarm(Schedule p1_sched)
	{
		Log.d(TAG, "addAlarm()ing...");
		Log.d(TAG, "alarm = " +p1_sched.toString());
		grab_handle();

		String sql = "INSERT INTO scheduled_event VALUES ("
			+       "NULL, ?, ?, ?, ?, ?)";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] {
				"" +p1_sched.childId,
				"" +(p1_sched.getAlarmType().ordinal()),
				"" +(p1_sched.isActive() ? "1": "0"),
				"" +(p1_sched.doesRepeat() ? "1": "0"),
				"" +p1_sched.getEpochTime()
			}
		);
	}

	public void updateAlarm(Schedule p1_sched)
	{
		Log.d(TAG, "updateAlarm()ing...");
		Log.d(TAG, "schedule = " +p1_sched.toString());
		grab_handle();

		String sql = "UPDATE scheduled_event "
			+       "SET type = ?, "
			+           "active = ?, "
			+           "repeat = ?, "
			+           "occurrence = ? "
			+     "WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { 
				"" +p1_sched.getType().ordinal(), 
				"" +(p1_sched.isActive() ? "1": "0"),
				"" +(p1_sched.doesRepeat() ? "1": "0"),
				"" +p1_sched.getEpochTime(),
				"" +p1_sched.id
			}
		);
	}

	public void deleteAlarm(long p1_id)
	{
		Log.d(TAG, "deleteAlarm(" +p1_id +")ing...");
		grab_handle();
		String sql = "DELETE FROM scheduled_event WHERE _id = ?";
		Log.d(TAG, "sql: " +sql);
		dbh.execSQL(sql, new String[] { "" +p1_id });
	}

	public long lastAlarm()
	{
		long retval = -1;
		Log.d(TAG, "lastAlarm()ing...");
		String sql = "SELECT last_insert_rowid() FROM scheduled_event";
		Log.d(TAG, "sql: " +sql);
		Cursor c = dbh.rawQuery(sql, new String[] {});

		if (c.moveToFirst()) {
			retval = c.getInt(0);
		}
		return(retval);
	}

	public XYMultipleSeriesDataset getSummaryDataset(Child p1_child, 
			int p2_chart)
	{
		Log.d(TAG, "getSummaryDataset()ing...");
		grab_handle();
		XYMultipleSeriesDataset retval = new XYMultipleSeriesDataset();
		TimeSeries series = null;
		SimpleDateFormat dfmt = new SimpleDateFormat("yyyyMMdd");
		Date d = new Date(System.currentTimeMillis());
		Assert.assertTrue(p1_child != null);

		switch (p2_chart) {
		case Constants.CHART_DIAPERS:
			series = new TimeSeries(Constants.diaper_kstring);

			Log.d(TAG, "counting diapers per day (XYSeries)...");
			String sql = "SELECT COUNT(*) "
				+        "AS c, "
				+           "strftime('%Y%m%d', occurrence, "
				+                    "'unixepoch','localtime') "
				+        "AS d "
				+      "FROM diaper_event "
				+     "WHERE child = ? "
				+  "GROUP BY d";
			Log.d(TAG, "sql: " +sql);
			Cursor c = dbh.rawQuery(sql, new String[] {
				"" +p1_child.id });
		
			while (c.moveToNext()) {
				try {
					d = dfmt.parse(c.getString(
						c.getColumnIndex("d")));
				}
				catch (Throwable e) {
					Log.w(TAG, e);
				}
				series.add(d, c.getDouble(
					c.getColumnIndex("c")));
			}
			retval.addSeries(series);
			break;
		case Constants.CHART_NAPS:
			series = new TimeSeries(Constants.nap_kstring);

			Log.d(TAG, "sum of nap-times per day (XYSeries)...");
			sql = "SELECT SUM(naplen) "
				+        "AS c, "
				+           "strftime('%Y%m%d', begin, "
				+                    "'unixepoch','localtime') "
				+        "AS d "
				+      "FROM nap_event "
				+     "WHERE child = ? "
				+  "GROUP BY d";
			Log.d(TAG, "sql: " +sql);
			c = dbh.rawQuery(sql, new String[] { ""+p1_child.id });
		
			while (c.moveToNext()) {
				try {
					d = dfmt.parse(c.getString(
						c.getColumnIndex("d")));
				}
				catch (Throwable e) {
					Log.w(TAG, e);
				}
				series.add(d, c.getDouble(
					c.getColumnIndex("c")));
			}
			retval.addSeries(series);
			break;
		case Constants.CHART_BOTTLES:
			series = new TimeSeries(Constants.milk_kstring);

			Log.d(TAG, "counting ounces per day (XYSeries)...");
			sql = "SELECT SUM(ounces) "
				+        "AS c, "
				+           "strftime('%Y%m%d', begin, "
				+                    "'unixepoch','localtime') "
				+        "AS d "
				+      "FROM bottle_event "
				+     "WHERE child = ? "
				+  "GROUP BY d";
			Log.d(TAG, "sql: " +sql);
			c = dbh.rawQuery(sql, new String[] { ""+p1_child.id });
		
			while (c.moveToNext()) {
				try {
					d = dfmt.parse(c.getString(
						c.getColumnIndex("d")));
				}
				catch (Throwable e) {
					Log.w(TAG, e);
				}
				series.add(d, c.getDouble(
					c.getColumnIndex("c")));
			}
			retval.addSeries(series);
			break;
		default:
			Assert.assertTrue(false);
			break;
		}
		return(retval);
	}

	public long getIdFromCursor(Cursor p1_cursor, int p2_offset) {
		long retval = -1;
		Assert.assertTrue(p1_cursor != null);
		Assert.assertTrue(p2_offset >= 0);

		if (p1_cursor.moveToPosition(p2_offset)) {
			retval = p1_cursor.getLong(
					p1_cursor.getColumnIndex("_id"));
		}
		else {
			Log.w(TAG, "couldn't move to cursor pos " +p2_offset);
		}
		return(retval);
	}

	private void createDB()
	{
		Log.d(TAG, "createDB()ing...");
		grab_handle();

		String sql = "";
		try {
			sql = "CREATE TABLE child ("
				+            "_id INTEGER PRIMARY KEY, "
				+            "gender INTEGER NOT NULL, "
				+            "name VARCHAR(1024) NOT NULL"
				+");";
			Log.d(TAG, "sql: " +sql);
			dbh.execSQL(sql);

			sql = "CREATE TABLE diaper_event ("
				+            "_id INTEGER PRIMARY KEY, "
				+            "child INTEGER NOT NULL, "
				+            "occurrence DATE NOT NULL, "
				+            "poopy INTEGER, "
				+            "wet INTEGER"
				+");";
			Log.d(TAG, "sql: " +sql);
			dbh.execSQL(sql);

			sql = "CREATE TABLE severity ("
				+            "_id INTEGER PRIMARY KEY, "
				+            "name VARCHAR(10) NOT NULL"
				+");";
			Log.d(TAG, "sql: " +sql);
			dbh.execSQL(sql);

			for (Severity s : Severity.values()) {
				sql = "INSERT INTO severity VALUES ("
					+s.ordinal() +", '" +s +"')";
				Log.d(TAG, "sql: " +sql);
				dbh.execSQL(sql);
			}
			sql = "CREATE TABLE bottle_event ("
				+          "_id INTEGER PRIMARY KEY, "
				+          "child INTEGER NOT NULL, "
				+          "begin DATE NOT NULL, "
				+          "ounces INTEGER"
				+");";
			Log.d(TAG, "sql: " +sql);
			dbh.execSQL(sql);

			sql = "CREATE TABLE nap_event ("
				+         "_id INTEGER PRIMARY KEY, "
				+         "child INTEGER NOT NULL, "
				+         "begin DATE NOT NULL, "
				+         "naplen INTEGER NOT NULL"
				+");";
			Log.d(TAG, "sql: " +sql);
			dbh.execSQL(sql);

			sql = "CREATE TABLE scheduled_event ("
				+         "_id INTEGER PRIMARY KEY, "
				+         "child INTEGER NOT NULL, "
				+         "type INTEGER NOT NULL, "
				+         "active INTEGER NOT NULL, "
				+         "repeat INTEGER NOT NULL, "
				+         "occurrence DATE NOT NULL"
				+");";
			Log.d(TAG, "sql: " +sql);
			dbh.execSQL(sql);

			sql = "CREATE TABLE scheduled_event_type ("
				+         "_id INTEGER PRIMARY KEY, "
				+         "name VARCHAR(255) NOT NULL"
				+");";
			Log.d(TAG, "sql: " +sql);
			dbh.execSQL(sql);

			for (Schedule.alarmType t:Schedule.alarmType.values()) {
				sql = "INSERT INTO scheduled_event_type "
					+  "VALUES ("
					+               t.ordinal() +", "
					+               "\"" +t +"\""
					+         ");";
				Log.d(TAG, "sql: " +sql);
				dbh.execSQL(sql);
			}
			sql = "CREATE TABLE meal_event ("
				+              "_id INTEGER PRIMARY KEY, "
				+              "child INTEGER NOT NULL, "
				+              "begin DATE NOT NULL, "
				+              "left_secs INTEGER, "
				+              "right_secs INTEGER, "
				+              "last_breast INTEGER"
				+");";
			Log.d(TAG, "sql: " +sql);
			dbh.execSQL(sql);
		}
		catch (SQLException e) {
			Log.e(TAG, "sql error: " +e.toString());
		}
	}
}
