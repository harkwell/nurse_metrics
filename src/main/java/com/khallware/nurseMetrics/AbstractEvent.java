package com.khallware.nurseMetrics;

import android.util.Log;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import junit.framework.Assert;

public abstract class AbstractEvent implements Parcelable
{
	private static final String TAG = Constants.TAG_ABSTEVENT;

	public long childId = 0;
	public long id = 0;
	protected String occurrence = "";
	protected long mepochTime = -1;
	private int year = -1;
	private int month = -1;
	private int day = -1;
	private int hour = -1;
	private int minute = -1;
	private EventFactory.eventType type = null;

	public AbstractEvent()
	{
		Log.d(TAG, "AbstractEvent() constructor");
		setOccurrence();
	}

	protected AbstractEvent(Parcel p1_input)
	{
		Log.d(TAG, "AbstractEvent(Parcel) constructor");
		id = p1_input.readLong();
		childId = p1_input.readLong();
		mepochTime = p1_input.readLong();
		type = EventFactory.eventType.values()[p1_input.readInt()];
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(mepochTime));
		setOccurrence(
			cal.get(Calendar.YEAR),
			cal.get(Calendar.MONTH) + 1,    // zero based
			cal.get(Calendar.DAY_OF_MONTH),
			cal.get(Calendar.HOUR_OF_DAY),
			cal.get(Calendar.MINUTE)
		);
	}

	public int describeContents()
	{
		return(0);
	}

	public void writeToParcel(Parcel p1_out, int p2_flags)
	{
		Log.d(TAG, "writeToParcel()ing...");
		p1_out.writeLong(id);
		p1_out.writeLong(childId);
		p1_out.writeLong(mepochTime);
		p1_out.writeInt(type.ordinal());
	}

	public boolean updateable()
	{
		boolean retval = (readyToAdd() && id > 0);
		Log.d(TAG, "AbstractEvent " +((retval)?"":"not ")
			+"updateable for the DB");
		return(retval);
	}

	public boolean readyToAdd()
	{
		boolean retval = true;
		retval = (retval && mepochTime > 0);
		Log.d(TAG, "AbstractEvent is " +((retval)?"":"not ") 
			+"ready to be added to DB");
		return(retval);
	}

	public void setType(EventFactory.eventType p1_type)
	{
		Log.d(TAG, "setType()ing...");
		Assert.assertTrue(p1_type != null);

		type = p1_type;
	}

	public EventFactory.eventType getType()
	{
		Assert.assertTrue(type != null);

		return(type);
	}

	public String getOccurrence()
	{
		return(occurrence);
	}

	public long getEpochTime()
	{
		return(mepochTime/1000);
	}

	public long getMEpochTime()
	{
		return(mepochTime);
	}

	public void setEpochTime(long p1_time)
	{
		setMEpochTime(p1_time*1000);
	}

	public void setMEpochTime(long p1_time)
	{
		mepochTime = p1_time;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(mepochTime));
		setOccurrence(
			cal.get(Calendar.YEAR),
			cal.get(Calendar.MONTH) + 1,    // zero based
			cal.get(Calendar.DAY_OF_MONTH),
			cal.get(Calendar.HOUR_OF_DAY),
			cal.get(Calendar.MINUTE)
		);
	}

	public synchronized void setOccurrence()
	{
		Calendar cal = Calendar.getInstance();
		setOccurrence(
			cal.get(Calendar.YEAR),
			cal.get(Calendar.MONTH) + 1,    // zero based
			cal.get(Calendar.DAY_OF_MONTH),
			cal.get(Calendar.HOUR_OF_DAY),
			cal.get(Calendar.MINUTE)
		);
		updateOccurrence();
	}

	public synchronized void setOccurrence(int p1_yr, int p2_mo, int p3_day)
	{
		Assert.assertTrue(p1_yr >= 1970);
		Assert.assertTrue(p2_mo >= 0 && p2_mo <= 11);
		Assert.assertTrue(p3_day > 0 && p3_day <= 31);

		year  = p1_yr;
		month = p2_mo;
		day   = p3_day;
		updateOccurrence();
	}

	public synchronized void setOccurrence(int p1_hr, int p2_min)
	{
		Assert.assertTrue(p1_hr >= 0 && p1_hr < 24);
		Assert.assertTrue(p2_min >= 0 && p2_min < 60);

		hour = p1_hr;
		minute = p2_min;
		updateOccurrence();
	}

	public synchronized void setOccurrence(int p1_yr, int p2_mo, 
			int p3_day, int p4_hr, int p5_min)
	{
		setOccurrence(p1_yr, p2_mo, p3_day);
		setOccurrence(p4_hr, p5_min);
		updateOccurrence();
	}

	public synchronized int getYear()
	{
		Assert.assertTrue(year >= 0);
		return(year);
	}

	public synchronized int getMonth()
	{
		Assert.assertTrue(month >= 0);
		return(month);
	}

	public synchronized int getDay()
	{
		Assert.assertTrue(day >= 0);
		return(day);
	}

	public synchronized int getHour()
	{
		Assert.assertTrue(hour >= 0);
		return(hour);
	}

	public synchronized int getMin()
	{
		Assert.assertTrue(minute >= 0);
		return(minute);
	}

	private void updateOccurrence()
	{
		Date d = new GregorianCalendar(year, month -1, day, hour,
			minute).getTime();
		occurrence = new SimpleDateFormat("MM/dd/yyyy hh:mm").format(d);
		mepochTime = d.getTime();
		Log.d(TAG, "event's occurrence date was updated");
	}
}
