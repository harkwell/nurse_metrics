package com.khallware.nurseMetrics;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Child implements Parcelable
{
	private static final String TAG = Constants.TAG_CHILD;
	public static enum gender { UNKNOWN, BOY, GIRL }

	public long id = 0;
	public String name = "";
	protected gender sex = gender.UNKNOWN;
	public static final Parcelable.Creator<Child> CREATOR = 
		new Parcelable.Creator<Child>() {
			public Child createFromParcel(Parcel p1_input) {
				return(new Child(p1_input));
			}
			public Child[] newArray(int p1_size) {
				return(new Child[p1_size]);
			}
		};

	public Child()
	{
		Log.d(TAG, "Child() constructor");
	}

	public Child(Parcel p1_input)
	{
		Log.d(TAG, "Child(Parcel) constructor");
		id = p1_input.readLong();
		name = p1_input.readString();
		sex = gender.values()[p1_input.readInt()];
	}

	public int describeContents()
	{
		return(0);
	}

	public void writeToParcel(Parcel p1_out, int p2_flags)
	{
		Log.d(TAG, "writeToParcel()ing...");
		p1_out.writeLong(id);
		p1_out.writeString(name);
		p1_out.writeInt(sex.ordinal());
	}

	public Child setName(String p1_name)
	{
		name = p1_name;
		return(this);
	}
	public Child setBoy()
	{
		sex = gender.BOY;
		return(this);
	}
	public Child setGirl()
	{
		sex = gender.GIRL;
		return(this);
	}
	public boolean isBoy()
	{
		return(sex == gender.BOY);
	}
	public boolean isGirl()
	{
		return(sex == gender.GIRL);
	}
	public boolean updateable()
	{
		return(id > 0 && readyToAdd());
	}
	public boolean readyToAdd()
	{
		boolean retval = (name.compareTo("") != 0);
		retval = (retval && sex != gender.UNKNOWN);
		Log.d(TAG, "child is " +((retval)?"":"not ")
			+"ready to be added to the DB");
		return(retval);
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append("id: ").append("" +id).append(", ")
			.append("name: ").append(name).append(", ")
			.append("gender: ").append(sex)
			.toString()
		);
	}
}
