package com.khallware.nurseMetrics;

import android.util.Log;
import android.widget.*;
import android.view.*;
import android.app.*;
import android.content.*;
import android.os.Bundle;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import junit.framework.Assert;

public class NurseMetrics extends Activity
{
	private static final String TAG = Constants.TAG_MAIN;
	private static final int RESULT_GETCHILD = 1;
	private static final int DIALOG_ABOUT = 1;
	private static final int MENU_ABOUT = 1;

	private OpenHelper dbh = null;
	private Context ctxPtr = null;
	private Child child = null;
	private Config config = null;
	private Spinner chartSpinner = null;
	private ArrayAdapter<String> charts = null;
	private boolean redirectCharts = false;

	@Override
	public void onCreate(Bundle p1_bundle)
	{
		Log.d(TAG, "onCreate()ing...");
		super.onCreate(p1_bundle);
		initConfig();
		
		dbh = OpenHelper.getHelper(this);
		setContentView(R.layout.main);
		charts = new ArrayAdapter<String>(
			this.getApplicationContext(),
			android.R.layout.simple_spinner_item,
			Constants.charts);
		charts.setDropDownViewResource(
				android.R.layout.simple_spinner_dropdown_item);
		chartSpinner = (Spinner)findViewById(R.id.chart_spinner);
		chartSpinner.setAdapter(charts);
		ctxPtr = this;
		initButtons();
	}

	@Override
	public void onResume()
	{
		Log.d(TAG, "onResume()ing...");
		super.onResume();
		redirectCharts = false;

		if (child == null) {
			child = new Child();
		}
	}

	private void initConfig()
	{
		Log.d(TAG, "initConfig()ing...");

		if (!(new File(Config.CONF_DIR +Config.CONF_FILE)).exists()) {
			Log.d(TAG, "copying config file from assets...");
			try {
				InputStream is = getAssets().open(
					Config.CONF_FILE);
				FileOutputStream os = new FileOutputStream(
					Config.CONF_DIR +Config.CONF_FILE);
				for (int c=0; (c=is.read()) != -1;) {
					os.write(c);
				}
			}
			catch (Throwable e) {
				Log.e(TAG, "failed to copy config file \""
					+Config.CONF_FILE +"\": " 
					+e.toString());
			}
		}
		config = Config.getConfig();
	}

	@Override
	public void onActivityResult(int p1_req, int p2_res, Intent p3_itn)
	{
		super.onActivityResult(p1_req, p2_res, p3_itn);
		Log.d(TAG, "got result from sub activity");

		switch (p1_req) {
		case RESULT_GETCHILD:
			if (p2_res != Activity.RESULT_OK) {
				break;
			}
			long id = p3_itn.getLongExtra(Constants.EXTRA_CHILD, 0);

			if (id <= 0) {
				Log.e(TAG, "No child selected");
				break;
			}
			child = dbh.getChild(id);
			int j = p3_itn.getIntExtra(Constants.EXTRA_DEST, -1);

			if (j < 0) {
				Log.e(TAG, "No event type returned, do charts");

				if (redirectCharts) {
					invokeChartScreen();
				}
				break;
			}
			invokeScreen(EventFactory.eventType.values()[j]);
			break;
		default:
			Assert.assertTrue(false); // : "no activity result";
		}
	}

	private void invokeScreen(EventFactory.eventType p1_e)
	{
		Log.d(TAG, "" +p1_e +" button pressed, opening activity");
		Intent i = null;
		Assert.assertTrue(p1_e != null);

		if (child.id > 0 && dbh.getChild(child.id).id > 0) {
			i = new Intent(ctxPtr, Constants.eventClass.get(p1_e));
			i.putExtra(Constants.EXTRA_CHILD, child.id);
			startActivity(i);
		}
		else {
			i = new Intent(ctxPtr, AddSelActivity.class);
			i.putExtra(Constants.EXTRA_DEST, p1_e.ordinal());
			startActivityForResult(i, RESULT_GETCHILD);
		}
	}

	private void invokeChartScreen()
	{
		Intent i;
		int p = chartSpinner.getSelectedItemPosition();
		redirectCharts = true;

		if (child.id > 0) {
			i = SummaryChart.render(ctxPtr, child, ++p);
			startActivity(i);
		}
		else {
			i = new Intent(ctxPtr, AddSelActivity.class);
			startActivityForResult(i, RESULT_GETCHILD);
		}
	}

	private void initButtons()
	{
		ImageButton btn_add_sel = (ImageButton)findViewById(
			R.id.btn_add_sel);
		ImageButton btn_diapers = (ImageButton)findViewById(
			R.id.btn_diapers);
		ImageButton btn_naps = (ImageButton)findViewById(
			R.id.btn_naps);
		ImageButton btn_schedule = (ImageButton)findViewById(
			R.id.btn_schedule);
		ImageButton btn_bottle = (ImageButton)findViewById(
			R.id.btn_bottle);
		ImageButton btn_feedtimes = (ImageButton)findViewById(
			R.id.btn_feedtimes);
		ImageButton btn_analysis = (ImageButton)findViewById(
			R.id.btn_analysis);
		btn_add_sel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				Intent i = new Intent(ctxPtr, 
					AddSelActivity.class);
				startActivityForResult(i, RESULT_GETCHILD);
			}
		});
		btn_diapers.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				invokeScreen(EventFactory.eventType.DIAPER);
			}
		});
		btn_naps.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				invokeScreen(EventFactory.eventType.NAP);
			}
		});
		btn_schedule.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				invokeScreen(EventFactory.eventType.SCHEDULE);
			}
		});
		btn_bottle.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				invokeScreen(EventFactory.eventType.BOTTLE);
			}
		});
		btn_feedtimes.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				invokeScreen(EventFactory.eventType.MEAL);
			}
		});

		btn_analysis.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				invokeChartScreen();
			}
		});
	}

	public boolean onCreateOptionsMenu(Menu p1_menu)
	{
		p1_menu.add(0, MENU_ABOUT, 0, "About");
		return(true);
	}

	public boolean onOptionsItemSelected(MenuItem p1_item)
	{
		switch (p1_item.getItemId()) {
		case MENU_ABOUT:
			Log.d(TAG, "user wants to view about");
			showDialog(DIALOG_ABOUT);
			break;
		default :
			Log.e(TAG, "unknown user opt: " +p1_item.getItemId());
			break;
		}
		return(true);
	}

	protected Dialog onCreateDialog(int p1_id)
	{
		Dialog retval = null;
		Log.d(TAG, "creating dialog " +p1_id);

		switch (p1_id) {
		case DIALOG_ABOUT:
			retval = new Dialog(this);
			retval.setContentView(R.layout.about);
			retval.setTitle("About");
			ImageView i = (ImageView)retval.findViewById(
				R.id.about_img);
			i.setImageResource(R.drawable.icon);
			break;
		default :
			Log.e(TAG, "unknown dialog: " +p1_id);
			break;
		}
		return(retval);
	}
}
