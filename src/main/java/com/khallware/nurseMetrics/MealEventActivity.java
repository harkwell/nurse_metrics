package com.khallware.nurseMetrics;

import android.util.Log;
import android.view.View;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.widget.*;
import junit.framework.Assert;
import java.util.regex.Pattern;
import java.util.TimerTask;
import java.util.Timer;

public class MealEventActivity extends AbstractEventActivity
{
	private final static String TAG = Constants.TAG_MEALACT;
	private static boolean isPaused = true;
	private static long suspendMEpoch = 0;
	private RadioButton rb_r = null;
	private RadioButton rb_l = null;
	private TextView leftSec = null;
	private TextView rightSec = null;
	private Timer mealTimer = new Timer();
	private Button cont = null;
	private Button pause = null;
	private Context ctxt = this;

	protected EventFactory.eventType getType()
	{
		return(EventFactory.eventType.MEAL);
	}

	protected void userPressedUpdate()
	{
		Meal tmp = dbh.getMeal(dbh.getIdFromCursor(cursor, 0));
		rb_r.setChecked(!tmp.wasLastBreastRight());
		rb_l.setChecked(tmp.wasLastBreastRight());
		readFromView();
		isPaused = true;
	}
	protected void userPressedDelete() { }

	protected void setupView()
	{
		if (Constants.withTitle) {
			setTitle(R.string.meal_act);
		}
		rb_r = (RadioButton)findViewById(R.id.rside);
		rb_l = (RadioButton)findViewById(R.id.lside);
		Meal m = (Meal)getEventRef();

		if (!m.wasLastBreastRight() && !m.wasLastBreastLeft()) {
			Meal tmp = dbh.getMeal(dbh.getIdFromCursor(cursor, 0));
			rb_r.setChecked(tmp.wasLastBreastLeft());
			rb_l.setChecked(tmp.wasLastBreastRight());
			m.setLastBreast((rb_r.isChecked())
				? Meal.breast.RIGHT : Meal.breast.LEFT);
		}
		leftSec = (TextView)findViewById(R.id.left_seconds);
		rightSec = (TextView)findViewById(R.id.right_seconds);
		cont = (Button)findViewById(R.id.cont);
		pause = (Button)findViewById(R.id.pause);

		cont.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				Log.d(TAG, "continue button clicked");
				readFromView();
				isPaused = false;
			}
		});
		pause.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				Log.d(TAG, "pause button clicked");
				isPaused = true;
			}
		});
		mealTimer.schedule(new TimerTask() {
			public void run()
			{
				((Activity)ctxt).runOnUiThread(mealTimerTick);
			}
		}, 0, 1000);
	}

	protected void readFromView()
	{
		Log.d(TAG, "readFromView()ing...");
		Assert.assertTrue(getEventRef() != null);
		Assert.assertTrue(leftSec != null);
		Assert.assertTrue(rightSec != null);

		String lsecsInput = leftSec.getText().toString();
		String rsecsInput = rightSec.getText().toString();
		boolean flag = true;
		((Meal)getEventRef()).setLastBreast(
			(rb_r.isChecked())?Meal.breast.RIGHT:Meal.breast.LEFT);

		if (lsecsInput.compareTo("") == 0) {
			Log.d(TAG, "no left seconds user input yet");
			flag = false;
		}
		else if (!Pattern.matches("^\\d*$", lsecsInput)) {
			warnMsg = Constants.WARN_INVALID;
		}
		else {
			((Meal)getEventRef()).setLeftSeconds(
				Integer.parseInt(lsecsInput));
			flag = false;
		}
		if (!flag && rsecsInput.compareTo("") == 0) {
			Log.d(TAG, "no right seconds user input yet");
			flag = false;
		}
		else if (!flag && !Pattern.matches("^\\d*$", rsecsInput)) {
			warnMsg = Constants.WARN_INVALID;
		}
		else if (!flag) {
			((Meal)getEventRef()).setRightSeconds(
				Integer.parseInt(rsecsInput));
			flag = false;
		}
		if (flag) {
			isPaused = true;
			Log.w(TAG, warnMsg);
			showDialog(WARN_DIALOG_ID);
		}
	}

	protected void writeToView()
	{
		Log.d(TAG, "writeToView()ing...");
		Assert.assertTrue(getEventRef() != null);
		Assert.assertTrue(leftSec != null);
		Assert.assertTrue(rightSec != null);

		leftSec.setText("" +((Meal)getEventRef()).getLeftSeconds());
		rightSec.setText("" +((Meal)getEventRef()).getRightSeconds());
		rb_r.setChecked(((Meal)eventRef).wasLastBreastRight());
		rb_l.setChecked(!((Meal)eventRef).wasLastBreastRight());
	}

	@Override
	public void onResume()
	{
		Log.d(TAG, "onResume()ing...");
		super.onResume();
		String input = "";
		int secs = 0;

		if (suspendMEpoch > 0) {
			secs = (int)((System.currentTimeMillis()-suspendMEpoch)/
				1000);
			Log.d(TAG, "" +secs +" secs during pause added back");

			if (rb_r.isChecked()) {
				input = rightSec.getText().toString();
			}
			else if (rb_l.isChecked()) {
				input = leftSec.getText().toString();
			}
			if (Pattern.matches("^\\d*$", input)) {
				secs += Integer.parseInt(input);
			}
			else {
				secs = 0;
				isPaused = true;
			}
			if (rb_r.isChecked()) {
				rightSec.setText("" +secs);
			}
			else if (rb_l.isChecked()) {
				leftSec.setText("" +secs);
			}
			suspendMEpoch = 0;
		}
	}

	@Override
	public void onPause()
	{
		Log.d(TAG, "onPause()ing...");
		super.onPause();

		if (!isPaused) {
			Log.d(TAG, "preserving current time while paused");
			suspendMEpoch = System.currentTimeMillis();
		}
	}

	private Runnable mealTimerTick = new Runnable() {
			public void run()
			{
				if (isPaused) {
					return;
				}
				int secs = 0;
				String input = "";

				if (rb_r.isChecked()) {
					input = rightSec.getText().toString();

					if (Pattern.matches("^\\d*$", input)) {
						secs = Integer.parseInt(input);
						secs++;
					}
					else {
						secs = 0;
						isPaused = true;
					}
					rightSec.setText("" +secs);
				}
				else if (rb_l.isChecked()) {
					input = leftSec.getText().toString();

					if (Pattern.matches("^\\d*$", input)) {
						secs = Integer.parseInt(input);
						secs++;
					}
					else {
						secs = 0;
						isPaused = true;
					}
					leftSec.setText("" +secs);
				}
			}
		};
}
