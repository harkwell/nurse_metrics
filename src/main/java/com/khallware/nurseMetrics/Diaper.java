package com.khallware.nurseMetrics;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Diaper extends AbstractEvent
{
	private static final String TAG = Constants.TAG_DIAPER;

	public Severity poopy = Severity.NONE;
	public Severity wet = Severity.NONE;

	public Diaper()
	{
		Log.d(TAG, "Diaper() constructor");
	}

	private Diaper(Parcel p1_input)
	{
		super(p1_input);
		Log.d(TAG, "Diaper(Parcel) constructor");
		poopy = Severity.values()[p1_input.readInt()];
		wet = Severity.values()[p1_input.readInt()];
	}

	@Override
	public void writeToParcel(Parcel p1_out, int p2_flags)
	{
		super.writeToParcel(p1_out, p2_flags);
		p1_out.writeInt(poopy.ordinal());
		p1_out.writeInt(wet.ordinal());
	}

	public static final Parcelable.Creator<Diaper> CREATOR = 
		new Parcelable.Creator<Diaper>() {
			public Diaper createFromParcel(Parcel p1_input) {
				return(new Diaper(p1_input));
			}
			public Diaper[] newArray(int p1_size) {
				return(new Diaper[p1_size]);
			}
		};

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append("type: ").append(getType()).append(", ")
			.append("id: ").append("" +id).append(", ")
			.append("child: ").append("" +childId).append(", ")
			.append("occurrence: ").append(occurrence).append(", ")
			.append("epoch: ").append("" +getEpochTime())
			.append(", ")
			.append("poopy: ").append("" +poopy).append(", ")
			.append("wet: ").append("" +wet)
			.toString()
		);
	}

}
