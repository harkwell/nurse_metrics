package com.khallware.nurseMetrics;

import android.app.*;
import android.os.Bundle;
import android.util.Log;
import android.widget.*;
import android.view.*;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.Calendar;
import junit.framework.Assert;

public abstract class AbstractEventActivity extends ListActivity
{
	private final static String TAG = Constants.TAG_EVENTACT;
	protected final static int TIME_DIALOG_ID = 0;
	protected final static int DATE_DIALOG_ID = 1;
	protected final static int WARN_DIALOG_ID = 2;
	protected final static int MENU_ITEM_DEL  = 1;
	protected final static int MENU_ITEM_EDIT = 2;

	protected OpenHelper dbh = null;
	protected AbstractEvent eventRef = null;
	protected String warnMsg = "";
	protected Cursor cursor = null;
	protected Child child = null;
	protected boolean useFutureDates = false;
	private ListAdapter adapter = null;

	abstract EventFactory.eventType getType();
	abstract void userPressedUpdate();
	abstract void userPressedDelete();
	abstract void setupView();
	abstract void readFromView();
	abstract void writeToView();

	@Override
	public void onCreate(Bundle p1_bundle)
	{
		Log.d(TAG, "onCreate()ing...");
		super.onCreate(p1_bundle);
		dbh = OpenHelper.getHelper(this);
		Assert.assertTrue(getType() != null);

		if (p1_bundle == null) {
			p1_bundle = getIntent().getExtras();
		}
		Assert.assertTrue(p1_bundle != null);

		if (child == null || child.id <= 0) {
			Log.d(TAG, "reading parcelable child, event of bundle");
			child = (Child)p1_bundle.getParcelable(
				Constants.EXTRA_CHILDOBJ);
			eventRef = (AbstractEvent)p1_bundle.getParcelable(
				Constants.EXTRA_EVENTOBJ);
		}
		if (child == null || child.id <= 0) {
			Log.d(TAG, "getting child from db denoted by saved id");
			long id = p1_bundle.getLong(Constants.EXTRA_CHILD);
			child = dbh.getChild(id);
		}
		if (eventRef == null || eventRef.childId <= 0) {
			Log.d(TAG, "creating a new " +getType() +" event obj");
			eventRef = EventFactory.createEvent(getType());
			eventRef.childId = child.id;
			eventRef.setOccurrence();
		}
		setTheme((child.isBoy())?R.style.BoyTheme:R.style.GirlTheme);
		Assert.assertTrue(eventRef != null);

		Log.d(TAG, "collecting " +getType() +" event data");
		setContentView(Constants.eventLayout.get(getType()));
		initButtons();
		updateAdapter();
		setupView();
		registerForContextMenu(getListView());
	}

	@Override
	public void onResume()
	{
		Log.d(TAG, "onResume()ing...");
		super.onResume();
		Assert.assertTrue(eventRef != null);

		updateAdapter();
		_writeToView();
		writeToView();
	}

	private void _userPressedUpdate()
	{
		boolean flag = false;
		_readFromView();
		readFromView();
		flag = (flag || !eventRef.readyToAdd());
		flag = (flag || !dbh.saveEvent(eventRef));

		if (flag) {
			warnMsg = "Update Failed, Bad Input";
			Log.d(TAG, warnMsg);
			showDialog(WARN_DIALOG_ID);
		}
		else {
			userPressedUpdate();
			eventRef = EventFactory.createEvent(getType());
			eventRef.childId = child.id;
			updateAdapter();
			_writeToView();
			writeToView();
		}
	}

	public AbstractEvent getEventRef()
	{
		return(eventRef);
	}

	protected void _writeToView()
	{
		Log.d(TAG, "_writeToView()ing...");
		TextView date = (TextView)findViewById(R.id.date);
		date.setText("" +eventRef.getOccurrence());
	}

	protected void _readFromView()
	{
		Log.d(TAG, "_readFromView()ing...");
		// the occurrence has already been set...
	}

	private void updateAdapter()
	{
		Log.d(TAG, "updateAdapter()ing...");
		Assert.assertTrue(eventRef != null);

		if (cursor != null) {
			stopManagingCursor(cursor);
		}
		cursor = dbh.queryEvent(eventRef);
		Assert.assertTrue(cursor != null);

		startManagingCursor(cursor);
		adapter = new SimpleCursorAdapter(
			this,
			R.layout.result_list_item,
			cursor,
			new String[] { Constants.RESULT_COLUMN },
			new int[] { R.id.summary }
		);
		setListAdapter(adapter);
	}

	private void initButtons() 
	{
		Button b1 = (Button)findViewById(R.id.cancel);
		b1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				Log.d(TAG, "cancel button clicked");
				finish();
			}
		});
		Button b2 = (Button)findViewById(R.id.pick_date);
		b2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				Log.d(TAG, "date button clicked");
				showDialog(DATE_DIALOG_ID);
			}
		});
		Button b3 = (Button)findViewById(R.id.pick_time);
		b3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				Log.d(TAG, "time button clicked");
				showDialog(TIME_DIALOG_ID);
			}
		});
		Button b4 = (Button)findViewById(R.id.done);
		b4.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				Log.d(TAG, "enter/update/done button clicked");
				_userPressedUpdate();
				// userPressedUpdate() called from above...
			}
		});
	}

	public void onListItemClick(ListView p1_l, View p2_v, int p3_pos, 
			long p4_id)
	{
		long event_id = dbh.getIdFromCursor(cursor, p3_pos);
		Log.d(TAG, "user chose event with id " +event_id);
		eventRef = dbh.getEvent(event_id, getType());
		_writeToView();
		writeToView();
	}

	@Override
	protected Dialog onCreateDialog(int p1_id)
	{
		Dialog retval = null;

		switch (p1_id) {
		case TIME_DIALOG_ID:
			retval = new TimePickerDialog(this, timeSetListener, 
				eventRef.getHour(), eventRef.getMin(), false);
			break;
		case DATE_DIALOG_ID:
			retval = new DatePickerDialog(this, dateSetListener,
				eventRef.getYear(), eventRef.getMonth()-1, 
				eventRef.getDay());
			break;
		case WARN_DIALOG_ID:
			retval = new Dialog(this);
			retval.setContentView(R.layout.warning);
			retval.setTitle(R.string.invalid_input);
			retval.setOnDismissListener(dialogDismissListener);
			break;
		}
		return(retval);
	}

	@Override
	public void onPause()
	{
		Log.d(TAG, "onPause()ing...");
		super.onPause();
	}

	@Override
	public void onSaveInstanceState(Bundle p1_bundle)
	{
		Log.d(TAG, "onSaveInstanceState()ing...");
		super.onSaveInstanceState(p1_bundle);
		_readFromView();
		readFromView();
		p1_bundle.putParcelable(Constants.EXTRA_CHILDOBJ, child);
		p1_bundle.putParcelable(Constants.EXTRA_EVENTOBJ, eventRef);
	}

	@Override
	public void onRestoreInstanceState(Bundle p1_bundle)
	{
		Log.d(TAG, "onRestoreInstanceState()ing...");
		super.onRestoreInstanceState(p1_bundle);
		child = (Child)p1_bundle.getParcelable(
			Constants.EXTRA_CHILDOBJ);
		eventRef = (AbstractEvent)p1_bundle.getParcelable(
			Constants.EXTRA_EVENTOBJ);
	}

	@Override
	protected void onPrepareDialog(int p1_id, Dialog p2_d)
	{
		switch (p1_id) {
		case TIME_DIALOG_ID:
			((TimePickerDialog)p2_d).updateTime(eventRef.getHour(), 
				eventRef.getMin());
			break;
		case DATE_DIALOG_ID:
			((DatePickerDialog)p2_d).updateDate(eventRef.getYear(),
				eventRef.getMonth()-1, eventRef.getDay());
			break;
		case WARN_DIALOG_ID:
			((Dialog)p2_d).setTitle(warnMsg);
			break;
		}
	}

	public void onCreateContextMenu(ContextMenu p1_menu, View p2_view,
			ContextMenuInfo p3_menuInfo)
	{
		Log.d(TAG, "onCreateContextMenu()ing...");
		super.onCreateContextMenu(p1_menu, p2_view, p3_menuInfo);
		p1_menu.add(0, MENU_ITEM_DEL, 0, "Delete");
		p1_menu.add(0, MENU_ITEM_EDIT, 0, "Edit");
	}

	public boolean onContextItemSelected(MenuItem p1_item)
	{
		Log.d(TAG, "onContextItemSelected()ing...");
		AdapterContextMenuInfo i = (AdapterContextMenuInfo)
			p1_item.getMenuInfo();
		long event_id = dbh.getIdFromCursor(cursor, i.position);
		eventRef = dbh.getEvent(event_id, getType());

		switch (p1_item.getItemId()) {
		case MENU_ITEM_DEL:
			Log.d(TAG, "user chose to del event id="+eventRef.id);
			userPressedDelete();
			dbh.deleteEvent(eventRef);
			eventRef = EventFactory.createEvent(getType());
			eventRef.childId = child.id;
			updateAdapter();
			break;
		case MENU_ITEM_EDIT:
			Log.d(TAG, "user chose to edit event id="+eventRef.id);
			_writeToView();
			writeToView();
			break;
		default:
			Log.d(TAG, "unknown context menu option");
			return(super.onContextItemSelected(p1_item));
		}
		return(true);
	}

	private TimePickerDialog.OnTimeSetListener timeSetListener =
		new TimePickerDialog.OnTimeSetListener() {
			public void onTimeSet(TimePicker p1_v, int p2_hour, 
					int p3_min)
			{
				Log.d(TAG, "timeSetListener called");
				_readFromView();
				readFromView();
				eventRef.setOccurrence(p2_hour, p3_min);
				_writeToView();
				writeToView();
			}
		};
	private DatePickerDialog.OnDateSetListener dateSetListener =
		new DatePickerDialog.OnDateSetListener() {
			public void onDateSet(DatePicker p1_v, int p2_year, 
					int p3_mo, int p4_day)
			{
				Log.d(TAG, "dateSetListener called");
				Calendar cal = Calendar.getInstance();
				boolean inFuture = (
					(p2_year > cal.get(Calendar.YEAR))
					|| (p2_year == cal.get(Calendar.YEAR)
					&& p3_mo > cal.get(Calendar.MONTH))
					|| (p2_year == cal.get(Calendar.YEAR)
					&& p3_mo == cal.get(Calendar.MONTH)
					&& p4_day > 
						cal.get(Calendar.DAY_OF_MONTH))
				);

				if (inFuture && !useFutureDates) {
					warnMsg = "Future Date Error";
					Log.d(TAG, warnMsg);
					showDialog(WARN_DIALOG_ID);
				}
				else if (!inFuture && useFutureDates) {
					warnMsg = "Date In Past Error";
					Log.d(TAG, warnMsg);
					showDialog(WARN_DIALOG_ID);
				}
				else {
					_readFromView();
					readFromView();
					eventRef.setOccurrence(p2_year, p3_mo+1,
						p4_day);
					_writeToView();
					writeToView();
				}
			}
		};
	private DialogInterface.OnDismissListener dialogDismissListener =
		new DialogInterface.OnDismissListener() {
			public void onDismiss(DialogInterface p1_dialog)
			{
				finish();
			}
		};
}
