package com.khallware.nurseMetrics;

import android.util.Log;
import android.view.View;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.os.Bundle;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import junit.framework.Assert;

public class DiaperEventActivity extends AbstractEventActivity
{
	private final static String TAG = Constants.TAG_DIAPERACT;
	private Spinner poopy = null;
	private Spinner wet = null;
	private String[] levels = null;
	private ArrayAdapter<String> sev = null;

	protected EventFactory.eventType getType()
	{
		return(EventFactory.eventType.DIAPER);
	}

	protected void userPressedUpdate() { }
	protected void userPressedDelete() { }

	protected void setupView()
	{
		poopy = (Spinner)findViewById(R.id.poopy);
		wet = (Spinner)findViewById(R.id.wet);
		levels = dbh.getSeverityLevels();
		Assert.assertTrue(getEventRef() != null);

		if (Constants.withTitle) {
			setTitle(R.string.diaper_act);
		}
		sev = new ArrayAdapter<String>(
			this.getApplicationContext(),
			android.R.layout.simple_spinner_item,
			levels);
		sev.setDropDownViewResource(
			android.R.layout.simple_spinner_dropdown_item);
		poopy.setAdapter(sev);
		wet.setAdapter(sev);
		poopy.setOnItemSelectedListener(poopyListener);
		poopy.setSelection(((Diaper)getEventRef()).poopy.ordinal());
		wet.setOnItemSelectedListener(wetListener);
		wet.setSelection(((Diaper)getEventRef()).wet.ordinal());
	}

	protected void readFromView()
	{
		Log.d(TAG, "readFromView()ing...");
		Assert.assertTrue(getEventRef() != null);

		Log.d(TAG, "poopy spinner " +(String)poopy.getSelectedItem());
		Log.d(TAG, "wet spinner " +(String)wet.getSelectedItem());

		((Diaper)getEventRef()).poopy = 
			Severity.values()[poopy.getSelectedItemPosition()];
		((Diaper)getEventRef()).wet = 
			Severity.values()[wet.getSelectedItemPosition()];
	}

	protected void writeToView()
	{
		Log.d(TAG, "writeToView()ing...");
		Assert.assertTrue(getEventRef() != null);
		Assert.assertTrue(sev != null);

		TextView date = (TextView)findViewById(R.id.date);
		date.setText("" +((Diaper)getEventRef()).getOccurrence());
		poopy.post(new Runnable() {
			public void run()
			{
				Log.d(TAG, "setting poopy via post to "
					+(String)poopy.getSelectedItem());
				poopy.setSelection(((Diaper)
					getEventRef()).poopy.ordinal());
			}
		});
		wet.post(new Runnable() {
			public void run()
			{
				Log.d(TAG, "setting wet via post to "
					+(String)wet.getSelectedItem());
				wet.setSelection(((Diaper)
					getEventRef()).wet.ordinal());
			}
		});
	}

	@Override
	public void onPause()
	{
		Log.d(TAG, "onPause()ing...");
		super.onPause();
	}

	private OnItemSelectedListener poopyListener =
		new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> p1_p,
					View p2_v, int p3_pos, long p4_id)
			{
				Log.d(TAG, "poopyness selected");
				readFromView();
			}
			public void onNothingSelected(AdapterView<?> p1_p)
			{
			}
		};
	private OnItemSelectedListener wetListener =
		new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> p1_p,
					View p2_v, int p3_pos, long p4_id)
			{
				Log.d(TAG, "wetness selected");
				readFromView();
			}
			public void onNothingSelected(AdapterView<?> p1_p)
			{
			}
		};
}
