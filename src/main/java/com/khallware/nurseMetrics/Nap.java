package com.khallware.nurseMetrics;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import junit.framework.Assert;

public class Nap extends AbstractEvent
{
	private static final String TAG = Constants.TAG_NAP;
	public  static final int MIN_TIME = Constants.MIN_NAP_TIME;
	public  static final int MAX_TIME = Constants.MAX_NAP_TIME;

	private int lengthMinutes = -1;

	public Nap()
	{
		Log.d(TAG, "constructing nap object");
		lengthMinutes = 0;
	}

	private Nap(Parcel p1_input)
	{
		super(p1_input);
		Log.d(TAG, "constructing nap object from Parcel");
		lengthMinutes = p1_input.readInt();
	}

	@Override
	public boolean readyToAdd()
	{
		boolean retval = super.readyToAdd();
		long now = Calendar.getInstance().getTime().getTime();
		retval = (retval && mepochTime < now);
		retval = (retval && lengthMinutes > 0);
		Log.d(TAG, "nap is " +((retval)?"":"not ")
			+"ready to be added to the DB");
		return(retval);
	}

	@Override
	public void writeToParcel(Parcel p1_out, int p2_flags)
	{
		Log.d(TAG, "writeToParcel()ing...");
		super.writeToParcel(p1_out, p2_flags);
		p1_out.writeInt(lengthMinutes);
	}

	public static final Parcelable.Creator<Nap> CREATOR = 
		new Parcelable.Creator<Nap>() {
			public Nap createFromParcel(Parcel p1_input) {
				return(new Nap(p1_input));
			}
			public Nap[] newArray(int p1_size) {
				return(new Nap[p1_size]);
			}
		};

	public void setLength(int p1_length)
	{
		if (p1_length > 0) {
			lengthMinutes = p1_length;
		}
	}

	public int getLength()
	{
		return(lengthMinutes);
	}

	public long getEndtimeAsLong()
	{
		Assert.assertTrue(lengthMinutes >= 0);

		GregorianCalendar cal = new GregorianCalendar(
			getYear(), getMonth()-1, getDay(), getHour(), 
			getMin());
		cal.add(Calendar.MINUTE, lengthMinutes);
		Nap n = new Nap();
		n.setOccurrence(
			cal.get(Calendar.YEAR),
			cal.get(Calendar.MONTH)+1, // zero based
			cal.get(Calendar.DAY_OF_MONTH),
			cal.get(Calendar.HOUR),
			cal.get(Calendar.MINUTE)
		);
		return(n.getEpochTime());
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append("type: ").append(getType()).append(", ")
			.append("id: ").append("" +id).append(", ")
			.append("began: ").append(occurrence).append(", ")
			.append("epoch: ").append(""+getEpochTime()+", ")
			.append("time: ").append("" +getLength())
			.toString()
		);
	}

}
