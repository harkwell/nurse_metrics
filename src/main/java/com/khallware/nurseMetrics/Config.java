package com.khallware.nurseMetrics;

import android.util.Log;
import android.content.Context;
import java.io.FileInputStream;
import java.util.Properties;

public class Config
{
	private static final String TAG = Constants.TAG_CONFIG;
	public static final String CONF_FILE = Constants.CONF_FILE;
	public static final String CONF_DIR = Constants.CONF_DIR;

	private static Config config = null;
	private static Properties props = null;

	private Config()
	{
		Log.d(TAG, "Config() constructor");
		props = new Properties();
		try {
			props.load(new FileInputStream(CONF_DIR +CONF_FILE));
		}
		catch (Throwable ex) {
			Log.e(TAG, Constants.ERR_FILEREAD +" \"" +CONF_FILE 
				+"\": " +ex.getMessage());
		}
	}

	public static synchronized Config getConfig()
	{
		if (config == null) {
			config = new Config();
		}
		return(config);
	}

	public String get(String p1_key)
	{
		return(props.getProperty(p1_key));
	}
}
