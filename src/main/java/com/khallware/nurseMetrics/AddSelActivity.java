package com.khallware.nurseMetrics;

import android.app.ListActivity;
import android.app.Activity;
import android.os.Bundle;
import android.database.Cursor;
import android.util.Log;
import android.widget.*;
import android.view.*;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.content.Intent;
import junit.framework.Assert;

public class AddSelActivity extends ListActivity
{
	private final static String TAG = Constants.TAG_ADDSEL;
	protected final static int MENU_ITEM_DEL = 1;
	protected final static int MENU_ITEM_EDIT = 2;

	private OpenHelper dbh = null;
	private Cursor cursor = null;
	private Child child = null;
	private Config config = null;
	private ListAdapter adapter = null;
	private int destination = -1;
	private EditText et = null;
	private RadioButton rb_b = null;
	private RadioButton rb_g = null;

	@Override
	public void onCreate(Bundle p1_bundle)
	{
		Log.d(TAG, "onCreate()ing...");
		super.onCreate(p1_bundle);
		config = Config.getConfig();
		child = new Child();
		dbh = OpenHelper.getHelper(this);
		setContentView(R.layout.add_select);
		et = (EditText)findViewById(R.id.name);
		rb_b = (RadioButton)findViewById(R.id.boy);
		rb_g = (RadioButton)findViewById(R.id.girl);

		if (p1_bundle == null) {
			p1_bundle = getIntent().getExtras();
		}
		if (p1_bundle != null) {
			destination = p1_bundle.getInt(Constants.EXTRA_DEST);
		}
		Button b1 = (Button)findViewById(R.id.done);
		b1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				Log.d(TAG, "done button clicked");
				boolean flag = false;
				child.name = et.getText().toString();
				child.setGirl();

				if (rb_b.isChecked()) {
					Log.d(TAG, "user selects a boy");
					child.setBoy();
				}
				else {
					Log.d(TAG, "user selects a girl");
					child.setGirl();
				}
				if (child.updateable()) {
					dbh.updateChild(child);
				}
				else if (dupChild()) {
					Log.w(TAG, "ignoring duplicate child");
				}
				else if (child.readyToAdd()) {
					dbh.addChild(child);
					child = dbh.getChildByName(child.name);
				}
				else {
					flag = true;
				}
				if (!flag) {
					sendResult();
				}
			}
		});
		Button b2 = (Button)findViewById(R.id.cancel);
		b2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View p1_v) {
				Log.d(TAG, "cancel button clicked");
				finish();
			}
		});
		rb_b.setChecked(child.isBoy());
		rb_g.setChecked(!child.isBoy());
		registerForContextMenu(getListView());
	}

	@Override
	public void onResume()
	{
		Log.d(TAG, "onResume()ing...");
		super.onResume();
		updateAdapter();
		updateChildView();
	}

	@Override
	public void onSaveInstanceState(Bundle p1_bundle)
	{
		super.onSaveInstanceState(p1_bundle);
		child.name = et.getText().toString();

		if (rb_b.isChecked()) {
			child.setBoy();
		}
		if (rb_g.isChecked()) {
			child.setGirl();
		}
		p1_bundle.putParcelable(Constants.EXTRA_CHILDOBJ, child);
	}

	@Override
	public void onRestoreInstanceState(Bundle p1_bundle)
	{
		super.onRestoreInstanceState(p1_bundle);
		child = (Child)p1_bundle.getParcelable(
			Constants.EXTRA_CHILDOBJ);
		et.setText(child.name);
		rb_b.setChecked(child.isBoy());
		rb_g.setChecked(!child.isBoy());
	}

	private void sendResult()
	{
		Log.d(TAG, "returning child as result: " +child.toString());
		setResult(Activity.RESULT_OK, 
			new Intent("").putExtra(Constants.EXTRA_CHILD, child.id)
				.putExtra(Constants.EXTRA_DEST, destination)
		);
		finish();
	}

	private void updateAdapter()
	{
		Log.d(TAG, "updateAdapter()ing...");

		if (cursor != null) {
			stopManagingCursor(cursor);
		}
		cursor = dbh.queryChildren();
		startManagingCursor(cursor);

		adapter = new SimpleCursorAdapter(
			this,
			R.layout.result_list_item,
			cursor,
			new String[] { Constants.RESULT_COLUMN },
			new int[] { R.id.summary }
		);
		setListAdapter(adapter);
	}

	private boolean dupChild()
	{
		boolean retval = true;
		retval = (retval && dbh.getChildByName(child.name).id > 0);
		Log.d(TAG, "this is " +((retval)?"":"not ") +"a dup child");
		return(retval);
	}

	public void onListItemClick(ListView p1_l, View p2_v, int p3_pos, 
			long p4_id)
	{
		Log.d(TAG, "list item selected");
		child = dbh.getChild(p4_id);
		sendResult();
	}

	private void updateChildView()
	{
		Log.d(TAG, "updateChildView()ing...");

		EditText name = (EditText)findViewById(R.id.name);
		name.setText(child.name);
		rb_b.setChecked(child.isBoy());
		rb_g.setChecked(!child.isBoy());
	}

	public void onCreateContextMenu(ContextMenu p1_menu, View p2_view,
			ContextMenuInfo p3_menuInfo)
	{
		Log.d(TAG, "onCreateContextMenu()ing...");
		super.onCreateContextMenu(p1_menu, p2_view, p3_menuInfo);
		p1_menu.add(0, MENU_ITEM_DEL, 0, "Delete");
		p1_menu.add(0, MENU_ITEM_EDIT, 0, "Edit");
	}

	public boolean onContextItemSelected(MenuItem p1_item)
	{
		Log.d(TAG, "onContextItemSelected()ing...");
		AdapterContextMenuInfo i = (AdapterContextMenuInfo)
			p1_item.getMenuInfo();
		long child_id = dbh.getIdFromCursor(cursor, i.position);
		child = dbh.getChild(child_id);

		switch (p1_item.getItemId()) {
		case MENU_ITEM_DEL:
			Log.d(TAG, "user chose to del child id=" +child_id);
			dbh.deleteChild(child_id);
			child = new Child();
			updateAdapter();
			break;
		case MENU_ITEM_EDIT:
			Log.d(TAG, "user chose to edit child id=" +child_id);
			updateChildView();
			break;
		default:
			Log.d(TAG, "unknown context menu option");
			return(super.onContextItemSelected(p1_item));
		}
		return(true);
	}
}
