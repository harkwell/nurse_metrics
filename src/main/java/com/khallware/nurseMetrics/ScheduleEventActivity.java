package com.khallware.nurseMetrics;

import android.util.Log;
import android.app.*;
import android.os.Bundle;
import android.widget.*;
import android.content.Intent;
import android.content.Context;
import android.view.View;
import android.database.Cursor;
import java.util.ArrayList;
import junit.framework.Assert;

public class ScheduleEventActivity extends AbstractEventActivity
{
	private final static String TAG = Constants.TAG_SCHEDACT;

	private CheckBox cb_repeat = null;
	private CheckBox cb_active = null;
	private Spinner spinner = null;
	private Schedule sched = null;

	protected EventFactory.eventType getType()
	{
		return(EventFactory.eventType.SCHEDULE);
	}

	protected void readFromView()
	{
		Assert.assertTrue(eventRef != null);

		((Schedule)eventRef).setRepeats(cb_repeat.isChecked());
		((Schedule)eventRef).setActive(cb_active.isChecked());
		int alarmTypeOrd = spinner.getSelectedItemPosition();
		((Schedule)eventRef).setAlarmType(
			Schedule.alarmType.values()[alarmTypeOrd]
		);
	}

	protected void writeToView()
	{
		withSchedCtx((Schedule)eventRef);
		Assert.assertTrue(eventRef != null);

		cb_repeat.setChecked(sched.doesRepeat());
		cb_active.setChecked(sched.isActive());
		spinner.setSelection(sched.getAlarmType().ordinal());

		if (sched.isActive() && !hasAlarm()) {
			Log.e(TAG, "schedule says active with no system alarm");
		}
		resetSchedCtx();
	}

	protected void setupView()
	{
		useFutureDates = true;
		dbh.cleanAlarms();
		cb_repeat = (CheckBox)findViewById(R.id.repeats);
		cb_repeat.setChecked(true);
		cb_active = (CheckBox)findViewById(R.id.active);
		cb_active.setChecked(false);
		ArrayList<String> spinVals = new ArrayList<String>();
		activateRepeatingAlarms();

		for (Schedule.alarmType v : Schedule.alarmType.values()) {
			spinVals.add("" +v);
		}
		spinner = (Spinner)findViewById(R.id.type_spinner);
		ArrayAdapter<String> types = new ArrayAdapter<String>(
				this.getApplicationContext(),
				android.R.layout.simple_spinner_item,
				spinVals
			);
		spinner.setAdapter(types);
		resetSchedCtx();
	}

	protected void userPressedUpdate()
	{
		Log.e(TAG, "processing user defined alarm");
		resetSchedCtx();

		if (((Schedule)eventRef).id <= 0) {
			withSchedCtx(dbh.getAlarm(dbh.lastAlarm()));
		}
		else {
			withSchedCtx((Schedule)eventRef);
		}
		Assert.assertTrue(sched != null);

		if (sched.isActive()) {
			sched.setAlarmTime(sched.getEpochTime());
			setAlarm();
		}
		else {
			delAlarm();
		}
		resetSchedCtx();
		dbh.cleanAlarms();
	}

	protected void userPressedDelete()
	{
		resetSchedCtx();

		if (((Schedule)eventRef).id <= 0) {
			withSchedCtx(dbh.getAlarm(dbh.lastAlarm()));
		}
		else {
			withSchedCtx((Schedule)eventRef);
		}
		delAlarm();
		resetSchedCtx();
	}

	@Override
	public void onSaveInstanceState(Bundle p1_bundle)
	{
		super.onSaveInstanceState(p1_bundle);
	}

	@Override
	public void onRestoreInstanceState(Bundle p1_bundle)
	{
		super.onRestoreInstanceState(p1_bundle);
	}

	private void setAlarm()
	{
		Assert.assertTrue(sched != null);
		Assert.assertTrue(cb_active != null);
		Assert.assertTrue(eventRef != null);

		if (sched.updateable() && sched.isActive()) {
			Intent i = new Intent(this, EventAlarmReceiver.class);
			i.putExtra(Constants.EXTRA_ALARM, sched);
			i.setExtrasClassLoader(getClassLoader());
			PendingIntent sender = PendingIntent.getBroadcast(this,
				(int)sched.id, i, 0);
			AlarmManager manager = (AlarmManager)
				getSystemService(Context.ALARM_SERVICE);

			if (sched.doesRepeat()) {
				manager.setRepeating(
					AlarmManager.RTC_WAKEUP,
					sched.getAlarmTime()*1000,
					24*60*60*1000,
					sender);
			}
			else {
				manager.set(
					AlarmManager.RTC_WAKEUP,
					sched.getAlarmTime()*1000,
					sender);
			}
			Log.d(TAG, "system alarm added with id " +sched.id);
		}
		else {
			Log.d(TAG, "alarm " +sched.id +" unready or inactive");
		}
	}

	private void delAlarm()
	{
		delAlarm(sched.id);
	}

	private void delAlarm(long p1_id)
	{
		Intent i = new Intent(this, EventAlarmReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(this,
			(int)p1_id, i, 0);
		AlarmManager manager = (AlarmManager)
			getSystemService(ALARM_SERVICE);
		manager.cancel(sender);
		Log.d(TAG, "alarm deleted with id " +p1_id);
	}

	private void withSchedCtx(Schedule p1_sched)
	{
		if (sched == null && p1_sched != null) {
			sched = p1_sched;
		}
	}

	private void activateRepeatingAlarms()
	{
		Cursor c = dbh.queryAlarms();
		Schedule tmp = sched;
		resetSchedCtx();

		for (int i=0; c != null && c.moveToNext(); i++) {
			long id = c.getLong(c.getColumnIndex("_id"));

			if (!hasAlarm(id)) {
				Schedule s = dbh.getAlarm(id);

				if (s.isActive()) {
					withSchedCtx(s);
					setAlarm();
					resetSchedCtx();
				}
			}
		}
		withSchedCtx(tmp);
	}

	private void resetSchedCtx()
	{
		sched = null;
	}

	private boolean hasAlarm()
	{
		return(hasAlarm(sched.id));
	}

	private boolean hasAlarm(long p1_id)
	{
		boolean retval = false;
		Intent i = new Intent(this, EventAlarmReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(this,
			(int)p1_id, i, PendingIntent.FLAG_NO_CREATE);
		retval = (sender != null);
		Log.d(TAG,"sched "+p1_id+" has "+((retval)?"":"no ") +"alarm");
		return(retval);
	}
}
