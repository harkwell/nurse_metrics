CREATE TABLE android_metadata (locale TEXT);
INSERT INTO android_metadata VALUES ("en_US");

CREATE TABLE child (
	_id INTEGER PRIMARY KEY,
	name VARCHAR(1024) NOT NULL
);

CREATE TABLE diaper_event (
	_id INTEGER PRIMARY KEY,
	child INTEGER NOT NULL,
	occurrence DATE NOT NULL,
	poopy ENUM ('major','average','minor'),
	wet ENUM ('major','average','minor')
);

CREATE TABLE bottle_event (
	_id INTEGER PRIMARY KEY,
	child INTEGER NOT NULL,
	begin DATE NOT NULL,
	endtime DATE,
	ounces INTEGER
);

CREATE TABLE nap_event (
	_id INTEGER PRIMARY KEY,
	child INTEGER NOT NULL,
	begin DATE NOT NULL,
	endtime DATE NOT NULL
);

CREATE TABLE scheduled_event (
	_id INTEGER PRIMARY KEY,
	child INTEGER NOT NULL,
	type INTEGER NOT NULL,
	occurrence DATE NOT NULL
);

CREATE TABLE scheduled_event_type (
	_id INTEGER PRIMARY KEY,
	name VARCHAR(255) NOT NULL
);
INSERT INTO scheduled_event_type VALUES (NULL, "Feeding Time");
INSERT INTO scheduled_event_type VALUES (NULL, "Nap Time");
INSERT INTO scheduled_event_type VALUES (NULL, "Diaper Check Time");

CREATE TABLE nurse_event (
	_id INTEGER PRIMARY KEY,
	child INTEGER NOT NULL,
	begin DATE NOT NULL,
	seconds INTEGER
);
